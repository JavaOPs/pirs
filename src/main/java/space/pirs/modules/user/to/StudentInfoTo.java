package space.pirs.modules.user.to;

import lombok.*;
import space.pirs.modules.user.model.Role;
import space.pirs.to.BaseTo;
import space.pirs.to.NamedTo;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true, exclude = {"password"})
@Builder(toBuilder = true)
public class StudentInfoTo extends BaseTo {
    private String name;
    private String email;
    private String password;
    private Set<Role> roles;

    private LocalDate birthday;
    private int startCareer;
    private NamedTo startTeam;
    private LocalDate startEducationDate;
    private LocalDate endEducationDate;
    private String phone;
    private String address;
    private String category;
    private Object reward;
    private int height;
    private int weight;
    private int footSize;
    private int chestGirth;
    private int waistGirth;
    private int hipGirth;
    private String imgName;

    private NamedTo coach;
    private NamedTo team;
}