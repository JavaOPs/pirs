package space.pirs.modules.training.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.model.AbstractBaseEntity;
import space.pirs.modules.skill.model.Skill;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@NoArgsConstructor
@Table(name = "training_item")
@Getter
@Setter
public class TrainingItem extends AbstractBaseEntity {

    @Column(name = "series")
    private int series;

    @Column(name = "repeat")
    private int repeat;

    @Column(name = "duration")
    private int duration;

    @Column(name = "pause")
    private int pause;

    @Column(name = "ordinal")
    @JsonIgnore
    private int ordinal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "training_id", nullable = false)
    @JsonIgnore
    @NotNull
    private Training training;

    @ManyToMany
    @JoinTable(
            name = "training_item_skill",
            joinColumns = @JoinColumn(name = "training_item_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id"))
    private Set<Skill> skills;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "exercise_id")
    private Exercise exercise;
}
