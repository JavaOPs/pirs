package space.pirs.modules.health.model;

public enum HealthState {
    REHABILITATION,
    TREATMENT,
    INJURY
}