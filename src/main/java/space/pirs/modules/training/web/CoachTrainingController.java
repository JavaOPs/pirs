package space.pirs.modules.training.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import space.pirs.AuthUser;
import space.pirs.modules.training.mapper.ExerciseShortMapper;
import space.pirs.modules.training.model.Training;
import space.pirs.modules.training.repository.ExerciseRepository;
import space.pirs.modules.training.repository.TrainingRepository;
import space.pirs.modules.training.service.TrainingService;
import space.pirs.modules.training.to.ExerciseShortTo;
import space.pirs.modules.training.to.TrainingInfoTo;

import javax.validation.Valid;
import java.util.List;

import static space.pirs.util.ValidationUtil.checkFoundByIdAndCoach;
import static space.pirs.util.ValidationUtil.checkUpdate;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = CoachTrainingController.REST_URL)
public class CoachTrainingController {
    public static final String REST_URL = "/api/coach/training";

    private final ExerciseShortMapper exerciseShortMapper;
    private final TrainingService service;
    private final TrainingRepository repository;
    private final ExerciseRepository exerciseRepository;

    @GetMapping("/{id}")
    public TrainingInfoTo get(@PathVariable int id, @AuthenticationPrincipal AuthUser coach) {
        log.info("get {} by {}", id, coach);
        return service.getById(id, coach.getUser());
   }

    @GetMapping("/exercises")
    public List<ExerciseShortTo> getExercises() {
        log.info("getExercises");
        return exerciseShortMapper.toToList(exerciseRepository.findAll());
    }

    @PutMapping("/{id}/passed")
    public void passed(@PathVariable int id, @RequestParam boolean passed, @AuthenticationPrincipal AuthUser coach) {
        log.info("passed {} {} by {}", id, passed, coach);
        checkUpdate(repository.passed(id, passed, coach.getUser()));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Training create(@RequestBody @Valid TrainingInfoTo to, @AuthenticationPrincipal AuthUser coach) {
        log.info("create {} ", to);
        return service.create(to, coach.getUser());
    }

    @PutMapping("/{id}")
    public Training update(@RequestBody @Valid TrainingInfoTo to, @PathVariable int id, @AuthenticationPrincipal AuthUser coach) {
        log.info("update {} {}", to, id);
        return service.update(to, id, coach.getUser());
    }

    @PutMapping("/{id}/activate")
    public void activate(@PathVariable int id, @RequestParam boolean activate, @AuthenticationPrincipal AuthUser coach) {
        log.info("activate {} ", id);
        checkFoundByIdAndCoach(repository.getByIdAndCoach(id, coach.getUser()), id, coach.getUser());
        repository.activate(id, activate);
    }
}
