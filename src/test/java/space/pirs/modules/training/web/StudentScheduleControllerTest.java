package space.pirs.modules.training.web;

import org.junit.Test;
import org.springframework.http.MediaType;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.training.to.ScheduleTo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.userAuth;
import static space.pirs.modules.training.TrainingTestData.STUDENT_SCHEDULES;
import static space.pirs.modules.training.TrainingTestData.contentJson;
import static space.pirs.modules.user.UserTestData.USER_STUDENT_1;

public class StudentScheduleControllerTest extends AbstractControllerTest {
    private static final String REST_URL = StudentScheduleController.REST_URL + "/";

    @Test
    public void testGetTrainingByMonth() throws Exception {
        mockMvc.perform(get(REST_URL)
                .param("month", "5")
                .with(userAuth(USER_STUDENT_1)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, STUDENT_SCHEDULES, ScheduleTo.class));
    }

    @Test
    public void testGetTrainingByMonthNotAuth() throws Exception {
        mockMvc.perform(get(REST_URL))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}