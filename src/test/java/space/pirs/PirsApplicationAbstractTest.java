package space.pirs;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import uk.co.jemos.podam.api.RandomDataProviderStrategy;
import uk.co.jemos.podam.api.RandomDataProviderStrategyImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class PirsApplicationAbstractTest {

	protected static PodamFactory factory;

	/**
	 * Добавление стратегий генерации значений и создание инстанса фабрики Podam.
	 * https://habr.com/ru/company/crystal_service/blog/255425/
	 */
	@Before
	public void setUp() {
		RandomDataProviderStrategy strategy = new RandomDataProviderStrategyImpl();
		factory = new PodamFactoryImpl(strategy);
	}
}
