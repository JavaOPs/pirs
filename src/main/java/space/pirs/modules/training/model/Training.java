package space.pirs.modules.training.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.model.AbstractTimestampEntry;
import space.pirs.modules.user.model.Team;
import space.pirs.modules.user.model.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

@Entity
@NoArgsConstructor
@Table(name = "training")
@Getter
@Setter
public class Training extends AbstractTimestampEntry {

    @Column(name = "date")
    @NotNull
    private LocalDate date;

    @Column(name = "start_time")
    @NotNull
    private LocalTime startTime;

    @Column(name = "end_time")
    @NotNull
    private LocalTime endTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "team_id")
    private Team team;

    @Column(name = "comment")
    @Size(max = 1024)
    private String comment;

    // https://stackoverflow.com/a/32150208/548473
    @OneToMany(mappedBy = "training", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private List<TrainingItem> items;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "training_student",
            joinColumns = @JoinColumn(name = "training_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id"))
    private Set<User> students;

    @Column(name = "passed")
    private boolean passed = false;
}
