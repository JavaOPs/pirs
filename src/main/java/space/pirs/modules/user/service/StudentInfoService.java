package space.pirs.modules.user.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import space.pirs.modules.user.mapper.StudentInfoMapper;
import space.pirs.modules.user.model.Role;
import space.pirs.modules.user.model.StudentDetails;
import space.pirs.modules.user.model.Team;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.StudentDetailsRepository;
import space.pirs.modules.user.repository.TeamRepository;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.modules.user.to.StudentInfoTo;
import space.pirs.service.UserValidationService;

import static space.pirs.util.ValidationUtil.checkFoundById;
import static space.pirs.util.ValidationUtil.checkRole;

@Service
@AllArgsConstructor
public class StudentInfoService {

    private final UserRepository userRepository;
    private final UserValidationService userValidationService;

    private final StudentDetailsRepository studentDetailsRepository;

    private final TeamRepository teamRepository;

    private final StudentInfoMapper studentInfoMapper;

    @Transactional
    public StudentDetails save(StudentInfoTo studentInfoTo) {
        checkRole(studentInfoTo.getRoles(), Role.ROLE_STUDENT);
        Team team = teamRepository.get(studentInfoTo.getTeam().getId());
        User student = new User(studentInfoTo.getId(), studentInfoTo.getEmail(),
                studentInfoTo.getName(),
                studentInfoTo.getPassword(),
                studentInfoTo.getRoles(), team);

        if (studentInfoTo.isNew()) {
            student = userRepository.createWithTimestamp(student);
        } else {
            student = userRepository.updateWithTimestamp(student, studentInfoTo.getId());
        }
        StudentDetails details = studentInfoMapper.toDetails(student, studentInfoTo);
        StudentDetails existDetails = studentDetailsRepository.findByUserId(student.id());
        if (existDetails != null) {
            details.setId(existDetails.id());
        }
        details = studentDetailsRepository.save(details);
        details.setStudent(student);
        student.setTeam(team);
        return details;
    }

    @Transactional
    public StudentInfoTo get(int studentId, int coachId) {
        StudentDetails studentDetails = checkFoundById(studentDetailsRepository.fetchWithUser(studentId), studentId);
        User student = studentDetails.getStudent();
        checkRole(student.getRoles(), Role.ROLE_STUDENT);
        if (coachId != -1) {
            userValidationService.checkStudentBelongCoach(student.id(), coachId);
        }
        return studentInfoMapper.toTo(studentDetails);
    }
}

