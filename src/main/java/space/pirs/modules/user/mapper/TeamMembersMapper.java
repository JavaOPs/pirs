package space.pirs.modules.user.mapper;

import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import space.pirs.BaseMapper;
import space.pirs.modules.user.model.Team;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.modules.user.to.TeamMembersTo;
import space.pirs.to.NamedTo;

@Mapper(componentModel = "spring")
public abstract class TeamMembersMapper implements BaseMapper<Team, TeamMembersTo> {
    @Autowired
    private UserRepository userRepository;

    protected User toStudents(NamedTo user) {
        return user == null ? null : userRepository.getOne(user.id());
    }
}
