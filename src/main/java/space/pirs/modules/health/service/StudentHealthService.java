package space.pirs.modules.health.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import space.pirs.AuthUser;
import space.pirs.modules.health.model.StudentHealth;
import space.pirs.modules.health.repository.StudentHealthRepository;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.service.UserValidationService;

import static space.pirs.util.ValidationUtil.checkFoundById;

@AllArgsConstructor
@Service
public class StudentHealthService {

    private final UserValidationService userValidationService;
    private final UserRepository userRepository;
    private final StudentHealthRepository studentHealthRepository;

    public StudentHealth get(int studentId) {
        return studentHealthRepository.getByStudentId(studentId);
    }

    @Transactional
    public StudentHealth get(int studentId, AuthUser coach) {
        userValidationService.checkStudentBelongCoach(studentId, coach.id());
        return studentHealthRepository.getByStudentId(studentId);
    }

    @Transactional
    public StudentHealth create(int studentId, StudentHealth studentHealth) {
        User student = userRepository.getStudent(studentId);
        studentHealth.setStudent(student);
        fillReferences(studentHealth);
        return studentHealthRepository.create(studentHealth);
    }

    @Transactional
    public StudentHealth update(int studentId, StudentHealth studentHealth, int id) {
        StudentHealth studentHealthDb = checkFoundById(studentHealthRepository.getByStudentId(studentId), studentId);
        studentHealthDb.setEvents(studentHealth.getEvents());
        studentHealthDb.setPolicies(studentHealth.getPolicies());
        fillReferences(studentHealthDb);
        return studentHealthRepository.update(studentHealthDb, id);
    }


    private void fillReferences(StudentHealth studentHealth) {
        if (studentHealth.getEvents() != null) {
            studentHealth.getEvents().forEach(he -> he.setStudentHealth(studentHealth));
        }
        if (studentHealth.getPolicies() != null) {
            studentHealth.getPolicies().forEach(ip -> ip.setStudentHealth(studentHealth));
        }
    }
}
