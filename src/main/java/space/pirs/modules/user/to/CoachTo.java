package space.pirs.modules.user.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.NamedImgTo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CoachTo extends NamedImgTo {
    public CoachTo(Integer id, String name, String imgName, LocalDateTime registered) {
        super(id, name, imgName);
        this.registered = registered.toLocalDate();
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDate registered;
    private List<TeamSizedTo> teams;
}
