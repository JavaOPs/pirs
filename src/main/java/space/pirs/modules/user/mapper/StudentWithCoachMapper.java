package space.pirs.modules.user.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import space.pirs.BaseMapper;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.to.StudentWithCoachTo;
import space.pirs.to.NamedTo;

@Mapper(componentModel = "spring")
public abstract class StudentWithCoachMapper implements BaseMapper<User, StudentWithCoachTo> {

    @Mappings({
            @Mapping(target = "coach", source = "team.coach", resultType = NamedTo.class),
            @Mapping(target = "registered", source = "startpoint")
    })
    public abstract StudentWithCoachTo toTo(User student);
}
