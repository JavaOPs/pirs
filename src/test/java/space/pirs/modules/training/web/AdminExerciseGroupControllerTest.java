package space.pirs.modules.training.web;

import org.junit.Test;
import org.springframework.http.MediaType;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.training.to.ExerciseGroupTo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.userAuth;
import static space.pirs.modules.training.TrainingTestData.EXERCISE_GROUPS;
import static space.pirs.modules.training.TrainingTestData.contentJson;
import static space.pirs.modules.user.UserTestData.ADMIN;

public class AdminExerciseGroupControllerTest extends AbstractControllerTest {
    private static final String REST_URL = AdminExerciseGroupController.REST_URL + "/";

    @Test
    public void testGetTree() throws Exception {
        mockMvc.perform(get(REST_URL + "tree")
                .with(userAuth(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, EXERCISE_GROUPS, ExerciseGroupTo.class));
    }

    @Test
    public void testGetTreeNotAuth() throws Exception {
        mockMvc.perform(get(REST_URL + "tree"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}