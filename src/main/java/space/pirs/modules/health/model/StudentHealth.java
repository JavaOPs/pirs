package space.pirs.modules.health.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import space.pirs.model.AbstractBaseEntity;
import space.pirs.modules.user.model.User;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "student_health")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StudentHealth extends AbstractBaseEntity {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id")
    @JsonIgnore
    private User student;

    @OneToMany(mappedBy = "studentHealth", cascade = CascadeType.PERSIST)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<InsurancePolicy> policies;

    @OneToMany(mappedBy = "studentHealth", cascade = CascadeType.PERSIST)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<HealthEvent> events;
}
