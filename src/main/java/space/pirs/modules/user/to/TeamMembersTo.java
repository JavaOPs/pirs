package space.pirs.modules.user.to;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.NamedTo;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TeamMembersTo extends TeamTo {
    public TeamMembersTo(TeamTo to, List<NamedTo> students) {
        this.id = to.getId();
        this.name = to.getName();
        this.coach = to.coach;
        this.students = List.copyOf(students);
    }

    private List<NamedTo> students;
}
