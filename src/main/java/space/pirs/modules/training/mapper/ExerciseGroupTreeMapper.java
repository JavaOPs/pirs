package space.pirs.modules.training.mapper;

import org.mapstruct.Mapper;
import space.pirs.BaseMapper;
import space.pirs.modules.training.model.ExerciseGroup;
import space.pirs.modules.training.to.ExerciseGroupTo;

@Mapper(componentModel = "spring", uses = {ExerciseMapper.class})
public abstract class ExerciseGroupTreeMapper implements BaseMapper<ExerciseGroup, ExerciseGroupTo> {
}
