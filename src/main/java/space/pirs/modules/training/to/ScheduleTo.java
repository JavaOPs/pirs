package space.pirs.modules.training.to;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.BaseTo;
import space.pirs.to.NamedTo;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
public class ScheduleTo extends BaseTo {
    private LocalDate date;
    private LocalTime startTime;
    private LocalTime endTime;
    private String comment;
    private NamedTo team;

    public ScheduleTo(Integer id, LocalDate date, LocalTime startTime, LocalTime endTime, String comment, NamedTo team) {
        super(id);
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.comment = comment;
        this.team = team;
    }
}
