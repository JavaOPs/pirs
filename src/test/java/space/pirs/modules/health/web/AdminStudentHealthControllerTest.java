package space.pirs.modules.health.web;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import space.pirs.AbstractControllerTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.userAuth;
import static space.pirs.modules.user.UserTestData.*;


public class AdminStudentHealthControllerTest extends AbstractControllerTest {
    private static final String REST_URL ="/api/admin/students/";

    @Test
    public void testGetStudentHealthUnauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(REST_URL + STUDENT_ID_1 + "/health"))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void testGetStudentHealthForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(REST_URL + STUDENT_ID_1 + "/health")
                .with(userAuth(COACH_1)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void testGet() throws Exception {
        mockMvc.perform(get(REST_URL + STUDENT_ID_1 + "/health")
                .with(userAuth(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
        //.andExpect(result -> assertMatch(jsonValueMapper.readValue (getContent(result), StudentTo.class), STUDENT_1));
    }

    @Test
    public void createStudentHealthTest() {
    }

    @Test
    public void updateStudentHealthTest() {
    }
}