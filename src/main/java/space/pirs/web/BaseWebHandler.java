package space.pirs.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import space.pirs.BaseMapper;
import space.pirs.model.AbstractBaseEntity;
import space.pirs.repository.AbstractRepository;
import space.pirs.to.BaseTo;

import java.util.List;

@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public abstract class BaseWebHandler<E extends AbstractBaseEntity, T extends BaseTo, R extends AbstractRepository<E>> {
    protected final R repository;
    protected final BaseMapper<E, T> mapper;
    protected final Logger log;

    public BaseWebHandler(R repository, BaseMapper<E, T> mapper, Class controllerClass) {
        this.repository = repository;
        this.mapper = mapper;
        log = LoggerFactory.getLogger(controllerClass);
    }

    public T get(int id) {
        log.info("get {}", id);
        return mapper.toTo(repository.get(id));
    }

    public List<T> getAll() {
        log.info("getAll");
        return mapper.toToList(repository.findAll());
    }

    public E create(T to) {
        log.info("create {}", to);
        return repository.create(mapper.toEntity(to));
    }

    public void update(T to, int id) {
        log.info("update {}, {}", to, id);
        repository.update(mapper.toEntity(to), id);
    }

    public void delete(int id) {
        log.info("delete {}", id);
        repository.deleteById(id);
    }
}