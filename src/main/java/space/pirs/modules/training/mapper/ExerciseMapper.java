package space.pirs.modules.training.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import space.pirs.BaseMapper;
import space.pirs.modules.training.model.Exercise;
import space.pirs.modules.training.to.ExerciseTo;

@Mapper(componentModel = "spring")
public abstract class ExerciseMapper implements BaseMapper<Exercise, ExerciseTo> {
    @Mappings({
            @Mapping(target = "skills", ignore = true),
            @Mapping(target = "circumstance", ignore = true)
    })
    public abstract ExerciseTo toTo(Exercise exercise);
}
