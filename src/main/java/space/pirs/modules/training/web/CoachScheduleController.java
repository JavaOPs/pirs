package space.pirs.modules.training.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import space.pirs.AuthUser;
import space.pirs.modules.training.mapper.ScheduleMapper;
import space.pirs.modules.training.repository.TrainingRepository;
import space.pirs.modules.training.service.TrainingService;
import space.pirs.modules.training.to.ScheduleTo;

import java.util.List;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = CoachScheduleController.REST_URL)
public class CoachScheduleController {
    public static final String REST_URL = "/api/coach/schedule";

    private final TrainingRepository repository;
    private final ScheduleMapper scheduleMapper;
    private final TrainingService service;

    @GetMapping
    public List<ScheduleTo> getTrainingByMonth(@RequestParam int month, @RequestParam(required = false) Integer studentId, @AuthenticationPrincipal AuthUser coach) {
        log.info("getTrainingByMonth {} by {}", month, coach);
        if (studentId == null) {
            return scheduleMapper.toToList(repository.getTrainingByCoachAndMonth(coach.getUser(), month));
        } else {
            return scheduleMapper.toToList(service.getByStudentAndCoachAndMonth(coach.getUser(), studentId, month));
        }
    }
}
