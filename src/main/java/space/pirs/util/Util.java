package space.pirs.util;

import org.springframework.util.Assert;
import space.pirs.HasId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class Util {

    public static <E extends HasId> List<E> order(Set<E> set) {
        List<E> list = new ArrayList<>(set);
        Collections.sort(list);
        return list;
    }

    public static <T> T getOneOrNull(List<T> list) {
        Assert.isTrue(list.size() <= 1, "list.size() > 1: " + list);
        return list.isEmpty() ? null : list.get(0);
    }
}
