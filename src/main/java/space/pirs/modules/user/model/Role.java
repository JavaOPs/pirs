package space.pirs.modules.user.model;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ROLE_ADMIN,
    ROLE_COACH,
    ROLE_STUDENT;

    @Override
    public String getAuthority() {
        return name();
    }
}