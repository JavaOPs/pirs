package space.pirs.modules.user.web;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.util.NestedServletException;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.user.mapper.StudentInfoMapper;
import space.pirs.modules.user.mapper.StudentMapper;
import space.pirs.modules.user.model.Role;
import space.pirs.modules.user.model.StudentDetails;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.StudentDetailsRepository;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.modules.user.to.StudentInfoTo;
import space.pirs.modules.user.to.StudentWithCoachTo;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.getContent;
import static space.pirs.TestUtil.userHttpBasic;
import static space.pirs.modules.user.UserTestData.*;

public class AdminStudentControllerTest extends AbstractControllerTest {

    private static final String REST_URL = AdminStudentController.REST_URL + "/";

    @Autowired
    private UserRepository repository;
    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private StudentDetailsRepository studentDetailsRepository;
    @Autowired
    private StudentInfoMapper studentInfoMapper;

    @Test
    public void testGetNotAuth() throws Exception {
        mockMvc.perform(get(REST_URL))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testGetAll() throws Exception {
        mockMvc.perform(get(REST_URL)
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, STUDENTS_WITH_COACH, StudentWithCoachTo.class));
    }

    @Test
    public void testDisActivate() throws Exception {
        mockMvc.perform(put(REST_URL + STUDENT_ID_1 + "/activate")
                .param("activate", "false")
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk());
        assertMatch(studentMapper.toToList(repository.findAllByRoleWithTeamAndCoach(Role.ROLE_STUDENT)), STUDENT_2, STUDENT_3);
    }

    @Test
    public void testGetByTeam() throws Exception {
        mockMvc.perform(get(REST_URL + "by-team")
                .param("teamId", String.valueOf(TEAM_ID_1))
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, List.of(STUDENT_WITH_COACH_1, STUDENT_WITH_COACH_2), StudentWithCoachTo.class));
    }

    @Test
    public void testGetByCoach() throws Exception {
        mockMvc.perform(get(REST_URL + "by-coach")
                .param("coachId", String.valueOf(COACH_ID_1))
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, STUDENTS_WITH_COACH, StudentWithCoachTo.class));
    }

    @Test
    public void testGet() throws Exception {
        mockMvc.perform(get(REST_URL + STUDENT_ID_1)
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertMatch(jsonValueMapper.readValue(getContent(result), StudentInfoTo.class), STUDENT_INFO_1, "password"));
    }

    @Test(expected = NestedServletException.class)
    public void testGetNotFound() throws Exception {
        mockMvc.perform(get(REST_URL + "0")
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void testUpdate() throws Exception {
        StudentInfoTo updated = NEW_STUDENT.toBuilder().build();
        updated.setId(STUDENT_ID_1);
        mockMvc.perform(put(REST_URL + STUDENT_ID_1)
                .with(userHttpBasic(ADMIN))
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValueMapper.writeValue(updated)))
                .andExpect(status().isOk());

        assertMatch(studentInfoMapper.toTo(studentDetailsRepository.findByUserId(STUDENT_ID_1)), updated, "reward");
    }

    @Test
    public void testCreate() throws Exception {
        StudentInfoTo createdStudent = NEW_STUDENT.toBuilder().build();
        createdStudent.setId(null);
        User createdUser = new User(NEW_STUDENT_USER);

        ResultActions action = mockMvc.perform(post(REST_URL)
                .with(userHttpBasic(ADMIN))
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValueMapper.writeValue(createdStudent)));

        StudentDetails returned = jsonValueMapper.readValue(getContent(action.andReturn()), StudentDetails.class);

        createdStudent.setId(returned.getStudent().getId());
        createdUser.setId(returned.getId());
        StudentDetails expected = studentInfoMapper.toDetails(createdUser, createdStudent);
        expected.getStudent().setId(returned.getStudent().getId());

        assertMatch(returned, expected, "startTeam");
    }

}