package space.pirs;

import lombok.Getter;
import org.springframework.lang.NonNull;
import space.pirs.modules.user.model.Role;
import space.pirs.modules.user.model.User;
import space.pirs.util.ValidationUtil;

@Getter
public class AuthUser extends org.springframework.security.core.userdetails.User {
    @NonNull
    private User user;

    public AuthUser(User user) {
        super(user.getEmail(), user.getPassword(), user.getRoles());
        this.user = user;
    }

    public boolean hasRole(Role role) {
        return user.getRoles().contains(role);
    }

    public void checkRole(Role role) {
        ValidationUtil.checkRole(user.getRoles(), role);
    }

    public int id() {
        return user.id();
    }

    @Override
    public String toString() {
        return "AuthUser{" +
                "email='" + user.getEmail() + '\'' +
                ", id=" + user.getId() +
                ", roles=" + user.getRoles() +
                '}';
    }
}