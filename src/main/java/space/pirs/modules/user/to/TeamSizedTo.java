package space.pirs.modules.user.to;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.NamedImgTo;

@NoArgsConstructor
@Getter
@Setter
public class TeamSizedTo extends NamedImgTo {
    public TeamSizedTo(Integer id, String name, String imgName, int size) {
        super(id, imgName, name);
        this.size = size;
    }

    protected int size;
}
