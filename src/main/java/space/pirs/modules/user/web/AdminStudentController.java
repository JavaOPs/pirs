package space.pirs.modules.user.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import space.pirs.modules.user.mapper.StudentWithCoachMapper;
import space.pirs.modules.user.model.Role;
import space.pirs.modules.user.model.StudentDetails;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.modules.user.service.StudentInfoService;
import space.pirs.modules.user.to.StudentInfoTo;
import space.pirs.modules.user.to.StudentWithCoachTo;

import javax.validation.Valid;
import java.util.List;

import static space.pirs.util.ValidationUtil.assureIdConsistent;
import static space.pirs.util.ValidationUtil.checkNew;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = AdminStudentController.REST_URL)
public class AdminStudentController {
    static final String REST_URL = "/api/admin/students";

    private final UserRepository repository;
    private final StudentWithCoachMapper mapper;
    private final StudentInfoService studentInfoService;

    @GetMapping("/by-team")
    public List<StudentWithCoachTo> getByTeam(@RequestParam int teamId) {
        log.info("getByTeam {}", teamId);
        return mapper.toToList(repository.getAllWithCoachByTeamId(teamId));
    }

    @GetMapping("/by-coach")
    public List<StudentWithCoachTo> getByCoach(@RequestParam int coachId) {
        log.info("getByCoach {}", coachId);
        return mapper.toToList(repository.getAllWithCoachByCoachId(coachId));
    }

    @GetMapping
    public List<StudentWithCoachTo> getAll() {
        log.info("getAll");
        return mapper.toToList(repository.findAllByRoleWithTeamAndCoach(Role.ROLE_STUDENT));
    }

    @PutMapping("/{id}/activate")
    public void activate(@PathVariable int id, @RequestParam boolean activate) {
        log.info("activate {}, {}", id, activate);
        repository.activate(id, activate);
    }

    @GetMapping("/{studentId}")
    public StudentInfoTo get(@PathVariable int studentId) {
        log.info("get {}", studentId);
        return studentInfoService.get(studentId, -1);
    }

    @PostMapping
    public StudentDetails save(@RequestBody @Valid StudentInfoTo studentInfoTo) {
        log.info("save {}", studentInfoTo);
        checkNew(studentInfoTo);
        return studentInfoService.save(studentInfoTo);
    }

    @PutMapping(value = "/{studentId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(@PathVariable int studentId, @RequestBody @Valid StudentInfoTo studentInfoTo) {
        log.info("update {} for student {}", studentInfoTo, studentId);
        assureIdConsistent(studentInfoTo, studentId);
        studentInfoService.save(studentInfoTo);
    }
}
