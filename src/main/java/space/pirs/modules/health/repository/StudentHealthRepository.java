package space.pirs.modules.health.repository;


import org.springframework.transaction.annotation.Transactional;
import space.pirs.modules.health.model.StudentHealth;
import space.pirs.repository.AbstractRepository;

@Transactional(readOnly = true)
public interface StudentHealthRepository extends AbstractRepository<StudentHealth> {
    StudentHealth getByStudentId(int studentId);
}
