package space.pirs.modules.user.mapper;

import org.mapstruct.Mapper;
import space.pirs.BaseMapper;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.to.StudentTo;

@Mapper(componentModel = "spring")
public abstract class StudentMapper implements BaseMapper<User, StudentTo> {
}
