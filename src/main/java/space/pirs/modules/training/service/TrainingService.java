package space.pirs.modules.training.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import space.pirs.modules.training.mapper.TrainingInfoMapper;
import space.pirs.modules.training.model.Training;
import space.pirs.modules.training.model.TrainingItem;
import space.pirs.modules.training.repository.TrainingItemRepository;
import space.pirs.modules.training.repository.TrainingRepository;
import space.pirs.modules.training.to.TrainingInfoTo;
import space.pirs.modules.user.model.User;
import space.pirs.service.UserValidationService;

import java.util.List;

import static space.pirs.util.ValidationUtil.checkFoundByIdAndCoach;

@Service
@AllArgsConstructor
public class TrainingService {

    private TrainingRepository repository;
    private TrainingItemRepository itemRepository;
    private TrainingInfoMapper mapper;
    private UserValidationService userValidationService;

    @Transactional(readOnly = true)
    public TrainingInfoTo getById(int id, User coach) {
        Training training = checkFoundByIdAndCoach(repository.getByIdAndCoachWithInfo(id, coach), id, coach);
        List<TrainingItem> items = itemRepository.getWithSkillsAndExercise(training);
        training.setItems(items);
        return mapper.toTo(training);
    }

    @Transactional
    public Training create(TrainingInfoTo to, User coach) {
        userValidationService.checkTeamBelongCoach(to.getTeam().id(), coach);
        Training training = mapper.toEntity(to);
        List<TrainingItem> items = training.getItems();
        training.setItems(null);
        Training savedTraining = repository.createWithTimestamp(training);
        setTrainingForItems(savedTraining, items);
        return repository.save(savedTraining);
    }

    @Transactional
    public Training update(TrainingInfoTo to, int id, User coach) {
        checkFoundByIdAndCoach(repository.getByIdAndCoach(id, coach), id, coach);
        userValidationService.checkTeamBelongCoach(to.getTeam().id(), coach);
        Training training = mapper.toEntity(to);
        setTrainingForItems(training, training.getItems());
        return repository.updateWithTimestamp(training, id);
    }

    private void setTrainingForItems(Training training, List<TrainingItem> items) {
        for (int i = 0; i < items.size(); i++) {
            TrainingItem item = items.get(i);
            item.setTraining(training);
            item.setOrdinal(i);
        }
        training.setItems(items);
    }

    @Transactional(readOnly = true)
    public List<Training> getByStudentAndCoachAndMonth(User coach, int studentId, int month) {
        User student = userValidationService.checkStudentBelongCoach(studentId, coach.getId());
        return repository.getTrainingByStudentAndMonth(student, month);
    }
}
