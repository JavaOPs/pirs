package space.pirs.modules.user.mapper;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import space.pirs.PirsApplicationAbstractTest;
import space.pirs.modules.user.model.StudentDetails;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.to.StudentInfoTo;

import static org.junit.Assert.assertEquals;

@Ignore
//TODO: fix
// WARN uk.co.jemos.podam.api.PodamFactoryImpl.resortToExternalFactory:1000 Cannot instantiate a class interface java.io.DataInput. Resorting to uk.co.jemos.podam.api.NullExternalFactory external factory
// WARN uk.co.jemos.podam.api.NullExternalFactory.manufacturePojo:52 Cannot instantiate interface java.io.DataInput with arguments []. Returning null.
public class StudentInfoMapperTest extends PirsApplicationAbstractTest {

    @Autowired
    private StudentInfoMapper studentInfoMapper;

    @Test
    public void getStudentInfoToTest() {
        User student = factory.manufacturePojoWithFullData(User.class);
        StudentDetails studentDetails = factory.manufacturePojo(StudentDetails.class);
        studentDetails.setId(student.getId());
        studentDetails.setStudent(student);
        StudentInfoTo studentInfoTo = studentInfoMapper.toTo(studentDetails);

        assertEquals(studentInfoTo.getId(), student.getId());
        assertEquals(studentInfoTo.getName(), student.getName());
        assertEquals(studentInfoTo.getRoles(), student.getRoles());
        assertEquals(studentInfoTo.getEmail(), student.getEmail());
        assertEquals(studentInfoTo.getPassword(), student.getPassword());
        assertEquals(studentInfoTo.getTeam().getId(), student.getTeam().getId());
        assertEquals(studentInfoTo.getCoach().getId(), student.getTeam().getCoach().getId());

        assertEquals(studentInfoTo.getStartCareer(), studentDetails.getStartCareer());
        assertEquals(studentInfoTo.getStartEducationDate(), studentDetails.getStartEducationDate());
        assertEquals(studentInfoTo.getEndEducationDate(), studentDetails.getEndEducationDate());
        assertEquals(studentInfoTo.getPhone(), studentDetails.getPhone());
        assertEquals(studentInfoTo.getAddress(), studentDetails.getAddress());
        assertEquals(studentInfoTo.getCategory(), studentDetails.getCategory());
        assertEquals(studentInfoTo.getHeight(), studentDetails.getHeight());
        assertEquals(studentInfoTo.getWeight(), studentDetails.getWeight());
        assertEquals(studentInfoTo.getFootSize(), studentDetails.getFootSize());
        assertEquals(studentInfoTo.getChestGirth(), studentDetails.getChestGirth());
        assertEquals(studentInfoTo.getWaistGirth(), studentDetails.getWaistGirth());
        assertEquals(studentInfoTo.getHipGirth(), studentDetails.getHipGirth());
    }
}