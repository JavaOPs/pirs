package space.pirs.modules.user.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import space.pirs.modules.user.mapper.TeamMembersMapper;
import space.pirs.modules.user.model.Team;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.TeamRepository;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.modules.user.to.TeamMembersTo;

import java.util.Collection;

import static space.pirs.util.ValidationUtil.checkFoundById;

@AllArgsConstructor
@Service
public class TeamService {

    private final UserRepository userRepository;

    private final TeamRepository teamRepository;

    private final TeamMembersMapper teamMembersMapper;

    @Transactional
    public TeamMembersTo get(int teamId) {
        Team team = checkFoundById(teamRepository.getByIdWithStudents(teamId), teamId);
        return teamMembersMapper.toTo(team);
    }

    @Transactional
    public void update(TeamMembersTo teamMembersTo, int teamId) {
        checkFoundById(teamRepository.get(teamId), teamId);
        Team team = teamMembersMapper.toEntity(teamMembersTo);
        assignTeam(userRepository.getAllByTeamId(teamId), null);
        assignTeam(team.getStudents(), team);
        teamRepository.update(team, teamId);
    }

    @Transactional
    public Team create(TeamMembersTo teamMembersTo) {
        Team team = teamMembersMapper.toEntity(teamMembersTo);
        assignTeam(team.getStudents(), team);
        return teamRepository.create(team);
    }

    private void assignTeam(Collection<User> students, Team team) {
        students.forEach(s -> s.setTeam(team));
    }
}
