package space.pirs.modules.user.to;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.NamedImgTo;
import space.pirs.to.NamedTo;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CoachInfoTo extends NamedImgTo {
    public CoachInfoTo(Integer id, String name, String imgName, String email, String password) {
        super(id, name, imgName);
        this.email = email;
        this.password = password;
    }

    public CoachInfoTo(CoachInfoTo coach) {
        this(coach.id, coach.name, coach.imgName, coach.email, coach.password);
        this.teams = coach.teams;
    }

    @Email
    @NotEmpty
    @Size(max = 128)
    private String email;

    @NotEmpty
    @Size(max = 256)
    private String password;

    private List<NamedTo> teams;
}
