package space.pirs.modules.training.to;

import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.NamedTo;

import java.util.List;

@Setter
@NoArgsConstructor
public class ExerciseGroupTo extends NamedTo {
    private List<ExerciseTo> exercises;

    public ExerciseGroupTo(NamedTo namedTo, List<ExerciseTo> exercises) {
        this.id = namedTo.id();
        this.name = namedTo.getName();
        this.exercises = exercises;
    }
}
