package space.pirs.modules.health.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.pirs.AuthUser;
import space.pirs.modules.health.model.StudentHealth;
import space.pirs.modules.health.service.StudentHealthService;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = StudentHealthController.REST_URL)
public class StudentHealthController {

    static final String REST_URL = "/api/student/health";

    private final StudentHealthService studentHealthService;

    @GetMapping
    public StudentHealth getStudentHealthByStudent(@AuthenticationPrincipal AuthUser authUser) {
        log.info("get by {}", authUser.getUser());
        return studentHealthService.get(authUser.getUser().id());
    }
}
