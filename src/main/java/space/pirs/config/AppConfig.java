package space.pirs.config;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import space.pirs.modules.user.model.Role;
import space.pirs.repository.AppRepository;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.function.Supplier;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class AppConfig {
    private final AppRepository appRepository;

    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server h2Server() throws SQLException {
        log.info("Start H2 TCP server");
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9092");
    }

    @PostConstruct
    public void init() {
        refreshProjectCounter();
    }

    public static volatile AppCounters appCounters;

    @Value
    public static class AppCounters {
        private final int students;
        private final int coaches;
        private final int schools;
    }

    private void refreshProjectCounter() {
        log.info("refreshProjectCounter");
        Object[][] rows = appRepository.getAppCounter();
        appCounters = new Supplier() {
            @Override
            public AppCounters get() {
                return new AppCounters(
                        counter(Role.ROLE_STUDENT.name()),
                        counter(Role.ROLE_COACH.name()),
                        counter(AppRepository.SCHOOL));
            }

            private int counter(String group) {
                for (Object[] row : rows) {
                    if (group.equals(row[0])) {
                        return ((BigInteger) row[1]).intValue();
                    }
                }
                return 0;
            }
        }.get();
    }
}
