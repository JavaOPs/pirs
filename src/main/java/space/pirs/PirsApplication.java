package space.pirs;

import lombok.AllArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
@AllArgsConstructor
public class PirsApplication {

    public static void main(String[] args) {
        SpringApplication.run(PirsApplication.class, args);
    }
}
