package space.pirs.modules.user.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import space.pirs.modules.user.mapper.TeamMapper;
import space.pirs.modules.user.model.Team;
import space.pirs.modules.user.repository.TeamRepository;
import space.pirs.modules.user.service.TeamService;
import space.pirs.modules.user.to.TeamMembersTo;
import space.pirs.modules.user.to.TeamTo;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(value = AdminTeamController.REST_URL)
public class AdminTeamController {
    static final String REST_URL = "/api/admin/teams";

    private final TeamRepository repository;
    private final TeamMapper mapper;
    private final TeamService teamService;

    @GetMapping
    public List<TeamTo> getAll() {
        log.info("getAll");
        return mapper.toToList(repository.findAll());
    }

    @GetMapping("/by-coach")
    public List<TeamTo> getAllByCoach(@RequestParam int coachId) {
        log.info("getByCoach {}", coachId);
        return mapper.toToList(repository.findAllByCoachId(coachId));
    }

    @GetMapping("/{id}")
    public TeamMembersTo getWithMembers(@PathVariable int id) {
        log.info("getWithStudents {}", id);
        return teamService.get(id);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateWithMembers(@RequestBody @Valid TeamMembersTo to, @PathVariable int id) {
        log.info("updateWithMembers {} {}", to, id);
        teamService.update(to, id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Team createWithMembers(@RequestBody @Valid TeamMembersTo to) {
        log.info("createWithMembers {}", to);
        return teamService.create(to);
    }

    @PutMapping("/{id}/activate")
    public void activate(@PathVariable int id, @RequestParam boolean activate) {
        log.info("activate {}, {}", id, activate);
        repository.activate(id, activate);
    }
}