package space.pirs.modules.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.model.AbstractNamedEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "team", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"}, name = "team_unique_name")})
@Getter
@Setter
@NoArgsConstructor
public class Team extends AbstractNamedEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coach_id")
    @JsonIgnore
    private User coach;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "team", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnore
    private Set<User> students;
}
