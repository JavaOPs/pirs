package space.pirs.modules.health;

import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.ResultMatcher;
import space.pirs.mapper.JsonValueMapper;
import space.pirs.modules.health.model.*;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.to.StudentTo;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static space.pirs.TestUtil.getContent;

@Component
public class HealthTestData {

    public static final int STUDENT_HEALTH_1_ID = 1;
    public static final int STUDENT_HEALTH_2_ID = 2;

    private static final int POLICY_ID_1 = 1;
    private static final int POLICY_ID_2 = 2;
    private static final int POLICY_ID_3 = 3;

    private static final int EVENT_ID_1 = 1;
    private static final int EVENT_ID_2 = 2;
    private static final int EVENT_ID_3 = 3;
    private static final int EVENT_ID_4 = 4;
    private static final int EVENT_ID_5 = 5;
    private static final int EVENT_ID_6 = 6;
    private static final int EVENT_ID_7 = 7;
    private static final int EVENT_ID_8 = 8;

    private static final StudentHealth STUDENT_HEALTH_1 = new StudentHealth();
    private static final StudentHealth STUDENT_HEALTH_2 = new StudentHealth();

    private static final User STUDENT_1 = new User();
    private static final User STUDENT_2 = new User();

    private static final InsurancePolicy INSURANCE_POLICY_1 =
            new InsurancePolicy(PolicyType.OMS, "10011977",
                    LocalDate.of(2019, 1, 1),
                    LocalDate.of(2020, 1, 1),
                    STUDENT_HEALTH_1);
    private static final InsurancePolicy INSURANCE_POLICY_2 =
            new InsurancePolicy(PolicyType.DMS, "10123011977",
                    LocalDate.of(2019, 1, 1),
                    LocalDate.of(2021, 1, 1),
                    STUDENT_HEALTH_1);
    private static final InsurancePolicy INSURANCE_POLICY_3 =
            new InsurancePolicy(PolicyType.OMS, "20190503",
                    LocalDate.of(2019, 5, 3),
                    LocalDate.of(2021, 12, 2),
                    STUDENT_HEALTH_2);

    private static final HealthEvent EVENT_1 =
            new HealthEvent(HealthState.INJURY, "Травма колена", "Все сложно",
                    LocalDate.of(2019, 2, 22),
                    LocalDate.of(2019, 2, 22),
                    STUDENT_HEALTH_1);
    private static final HealthEvent EVENT_2 =
            new HealthEvent(HealthState.TREATMENT, "Травма колена", "Зашиваем. Накладываем гипс",
                    LocalDate.of(2019, 2, 23),
                    LocalDate.of(2019, 3, 21),
                    STUDENT_HEALTH_1);
    private static final HealthEvent EVENT_3 =
            new HealthEvent(HealthState.REHABILITATION, "Травма колена", "Понемногу расхаживаемся",
                    LocalDate.of(2019, 3, 22),
                    LocalDate.of(2019, 4, 30),
                    STUDENT_HEALTH_1);
    private static final HealthEvent EVENT_4 =
            new HealthEvent(HealthState.INJURY, "Травма плеча", "Все сложно опять",
                    LocalDate.of(2019, 4, 13),
                    LocalDate.of(2019, 4, 13),
                    STUDENT_HEALTH_1);
    private static final HealthEvent EVENT_5 =
            new HealthEvent(HealthState.TREATMENT, "Травма плеча", "Опять зашиваем и накладываем гипс",
                    LocalDate.of(2019, 4, 13),
                    LocalDate.of(2019, 4, 20),
                    STUDENT_HEALTH_1);
    private static final HealthEvent EVENT_6 =
            new HealthEvent(HealthState.REHABILITATION, "Травма плеча", "Опять понемногу расхаживаемся",
                    LocalDate.of(2019, 4, 21),
                    LocalDate.of(2019, 5, 20),
                    STUDENT_HEALTH_1);


    private static final HealthEvent EVENT_7 =
            new HealthEvent(HealthState.INJURY, "Травма плеча", "Вывих",
                    LocalDate.of(2019, 5, 13),
                    LocalDate.of(2019, 5, 13),
                    STUDENT_HEALTH_2);
    private static final HealthEvent EVENT_8 =
            new HealthEvent(HealthState.TREATMENT, "Травма плеча", "Вправляем. Повязка",
                    LocalDate.of(2019, 5, 13),
                    LocalDate.of(2019, 5, 23),
                    STUDENT_HEALTH_2);

    public static <T> void assertMatch(T actual, T expected) {
        assertThat(actual).isEqualToIgnoringGivenFields(expected, "student");
    }

    public static <T> void assertMatch(T actual, T expected, String... ignore) {
        assertThat(actual).isEqualToIgnoringGivenFields(expected, ignore);
    }

    public static <T> void assertMatch(Iterable<T> actual, Iterable<T> expected) {
        assertThat(actual).usingElementComparatorIgnoringFields("student").isEqualTo(expected);
    }

    public static <T> void assertMatch(Iterable<T> actual, T... expected) {
        assertMatch(actual, List.of(expected));
    }

    public static ResultMatcher contentJson(JsonValueMapper jsonValueMapper, StudentTo... expected) {
        return contentJson(jsonValueMapper, List.of(expected));
    }

    public static ResultMatcher contentJson(JsonValueMapper jsonValueMapper, Iterable<StudentTo> expected) {
        return result -> assertMatch(jsonValueMapper.readValues(getContent(result), StudentTo.class), expected);
    }
}
