package space.pirs.modules.health.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.pirs.AuthUser;
import space.pirs.modules.health.model.StudentHealth;
import space.pirs.modules.health.service.StudentHealthService;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = CoachStudentHealthController.REST_URL)
public class CoachStudentHealthController {

    static final String REST_URL = "/api/coach/students";

    private final StudentHealthService studentHealthService;

    @GetMapping("/{studentId}/health")
    public StudentHealth getStudentHealth(@PathVariable int studentId, @AuthenticationPrincipal AuthUser coach) {
        log.info("get studentId={}", studentId);
        return studentHealthService.get(studentId, coach);
    }
}
