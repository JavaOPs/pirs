package space.pirs.modules.training.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import space.pirs.model.AbstractRefEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@NoArgsConstructor
@Table(name = "exercise_group",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"name"}, name = "exercise_group_unique_name")})
public class ExerciseGroup extends AbstractRefEntity {
    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY)
    private Set<Exercise> exercises;

}