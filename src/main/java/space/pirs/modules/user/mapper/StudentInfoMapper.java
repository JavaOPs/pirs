package space.pirs.modules.user.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import space.pirs.mapper.JsonValueMapper;
import space.pirs.modules.user.model.StudentDetails;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.to.StudentInfoTo;

@Mapper(
        componentModel = "spring",
        uses = JsonValueMapper.class
)
public abstract class StudentInfoMapper {
    @Mappings({
            @Mapping(target = "id", source = "student.id"),
            @Mapping(target = "name", source = "student.name"),
            @Mapping(target = "email", source = "student.email"),
            @Mapping(target = "password", source = "student.password"),
            @Mapping(target = "roles", source = "student.roles"),
            @Mapping(target = "coach", source = "student.team.coach"),
            @Mapping(target = "team", source = "student.team"),
            @Mapping(target = "imgName", source = "student.imgName"),
    })
    public abstract StudentInfoTo toTo(StudentDetails details);

    @Mappings({
            @Mapping(target = "id", source = "student.id"),
            @Mapping(target = "student", source = "student"),
            @Mapping(target = "birthday", source = "studentInfoTo.birthday"),
            @Mapping(target = "startCareer", source = "studentInfoTo.startCareer"),
            @Mapping(target = "startTeam", source = "studentInfoTo.startTeam"),
            @Mapping(target = "startEducationDate", source = "studentInfoTo.startEducationDate"),
            @Mapping(target = "endEducationDate", source = "studentInfoTo.endEducationDate"),
            @Mapping(target = "phone", source = "studentInfoTo.phone"),
            @Mapping(target = "address", source = "studentInfoTo.address"),
            @Mapping(target = "category", source = "studentInfoTo.category"),
            @Mapping(target = "reward", source = "studentInfoTo.reward",
                    qualifiedByName = {"JsonValueMapper", "WriteValue"}),
            @Mapping(target = "height", source = "studentInfoTo.height"),
            @Mapping(target = "weight", source = "studentInfoTo.weight"),
            @Mapping(target = "footSize", source = "studentInfoTo.footSize"),
            @Mapping(target = "chestGirth", source = "studentInfoTo.chestGirth"),
            @Mapping(target = "hipGirth", source = "studentInfoTo.hipGirth"),
            @Mapping(target = "waistGirth", source = "studentInfoTo.waistGirth")
    })
    public abstract StudentDetails toDetails(User student, StudentInfoTo studentInfoTo);
}