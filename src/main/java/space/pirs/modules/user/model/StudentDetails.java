package space.pirs.modules.user.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.model.AbstractBaseEntity;

import javax.persistence.*;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(name = "student_details")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StudentDetails extends AbstractBaseEntity {

    @Column(name = "birthday")
    @PastOrPresent
    private LocalDate birthday;

    @Column(name = "start_career")
    private int startCareer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "start_team_id")
    private Team startTeam;

    @Column(name = "start_education_date")
    private LocalDate startEducationDate;

    @Column(name = "end_education_date")
    private LocalDate endEducationDate;

    @Column(name = "phone")
    @Size(max = 20)
    private String phone;

    @Column(name = "address")
    @Size(max = 256)
    private String address;

    @Column(name = "category")
    private String category;

    @Column(name = "reward")
    private String reward;

    @Column(name = "height")
    private int height;

    @Column(name = "weight")
    private int weight;

    @Column(name = "foot_size")
    private int footSize;

    @Column(name = "chest_girth")
    private int chestGirth;

    @Column(name = "waist_girth")
    private int waistGirth;

    @Column(name = "hip_girth")
    private int hipGirth;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id", unique = true, nullable = false)
    private User student;
}
