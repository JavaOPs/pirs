package space.pirs.modules.health.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.model.AbstractBaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(name = "health_event")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HealthEvent extends AbstractBaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "health_state", nullable = false)
    private HealthState healthState;

    @Column(name = "diagnosis", nullable = false)
    @NotEmpty
    @Size(max = 128)
    private String diagnosis;

    @Column(name = "comment")
    @Size(max = 256)
    private String comment;

    @Column(name = "start_date")
    @NotNull
    private LocalDate startDate;

    @Column(name = "end_date")
    @NotNull
    private LocalDate endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_health_id")
    @JsonIgnore
    StudentHealth studentHealth;
}
