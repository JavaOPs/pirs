package space.pirs.modules.user.web;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.user.service.TeamService;
import space.pirs.modules.user.to.TeamMembersTo;
import space.pirs.to.NamedTo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.getContent;
import static space.pirs.TestUtil.userHttpBasic;
import static space.pirs.modules.user.UserTestData.*;

public class AdminTeamControllerTest extends AbstractControllerTest {
    private static final String REST_URL = AdminTeamController.REST_URL + "/";

    @Autowired
    private TeamService teamService;

    @Test
    public void testGetByCoach() throws Exception {
        mockMvc.perform(get(REST_URL + "by-coach")
                .param("coachId", String.valueOf(COACH_ID_1))
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, COACHES_1_TEAMS, NamedTo.class));
    }

    @Test
    public void testGet() throws Exception {
        mockMvc.perform(get(REST_URL + TEAM_ID_1)
                .param("teamId", String.valueOf(TEAM_ID_1))
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertMatch(jsonValueMapper.readValue(getContent(result), TeamMembersTo.class), TEAM_1_WITH_MEMBERS));
    }

    @Test
    public void testUpdate() throws Exception {
        mockMvc.perform(put(REST_URL + TEAM_ID_1)
                .with(userHttpBasic(ADMIN))
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValueMapper.writeValue(TEAM_1_UPDATED_WITH_MEMBERS)))
                .andExpect(status().isOk());
        assertMatch(teamService.get(TEAM_ID_1), TEAM_1_UPDATED_WITH_MEMBERS);
    }
}