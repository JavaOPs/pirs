package space.pirs.modules.training.repository;

import org.springframework.data.jpa.repository.Query;
import space.pirs.modules.training.model.ExerciseGroup;
import space.pirs.repository.AbstractRepository;

import java.util.List;

public interface ExerciseGroupRepository extends AbstractRepository<ExerciseGroup> {

    @Query("SELECT DISTINCT g FROM ExerciseGroup g LEFT JOIN FETCH g.exercises")
    List<ExerciseGroup> getAllWithExercises();
}