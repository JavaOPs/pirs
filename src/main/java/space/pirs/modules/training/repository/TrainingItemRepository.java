package space.pirs.modules.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import space.pirs.modules.training.model.Training;
import space.pirs.modules.training.model.TrainingItem;

import java.util.List;

public interface TrainingItemRepository extends JpaRepository<TrainingItem, Integer> {

    @Query("SELECT i FROM TrainingItem i LEFT JOIN FETCH i.skills LEFT JOIN FETCH i.exercise WHERE i.training =:training ORDER BY i.ordinal")
    List<TrainingItem> getWithSkillsAndExercise(Training training);
}
