package space.pirs.modules.health.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import space.pirs.modules.health.model.StudentHealth;
import space.pirs.modules.health.service.StudentHealthService;

import javax.validation.Valid;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api/admin/students/{studentId}/health")
public class AdminStudentHealthController {

    private final StudentHealthService studentHealthService;

    @GetMapping
    public StudentHealth get(@PathVariable int studentId) {
        log.info("get {}", studentId);
        return studentHealthService.get(studentId);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public StudentHealth create(@PathVariable int studentId, @RequestBody @Valid StudentHealth studentHealth) {
        log.info("create {} {}", studentId, studentHealth);
        return studentHealthService.create(studentId, studentHealth);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public StudentHealth update(@PathVariable int studentId, @RequestBody @Valid StudentHealth studentHealth, @PathVariable int id) {
        log.info("update {} {}", studentId, studentHealth);
        return studentHealthService.update(studentId, studentHealth, id);
    }
}
