package space.pirs.modules.user.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import space.pirs.modules.user.mapper.CoachInfoMapper;
import space.pirs.modules.user.model.Team;
import space.pirs.modules.user.model.TeamSizedWithCoach;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.TeamRepository;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.modules.user.to.CoachInfoTo;
import space.pirs.modules.user.to.CoachTo;
import space.pirs.modules.user.to.TeamSizedTo;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@AllArgsConstructor
@Service
@Transactional(readOnly = true)
public class CoachService {

    private final TeamRepository teamRepository;
    private final UserRepository userRepository;
    private final CoachInfoMapper coachInfoMapper;

    @Transactional
    public List<CoachTo> getAll() {
        List<TeamSizedWithCoach> teamsWithSize = teamRepository.getAllWithSize();
        Map<CoachTo, List<TeamSizedTo>> coachesMap = teamsWithSize.stream()
                .collect(Collectors.groupingBy(
                        ts -> new CoachTo(ts.getCoachId(), ts.getCoachName(), ts.getCoachImgName(), ts.getRegistered()),
                        filtering(ts -> ts.getId() != null,
                                mapping(ts -> new TeamSizedTo(ts.getId(), ts.getName(), ts.getCoachImgName(), ts.getSize()), toList()))));

        coachesMap.forEach(CoachTo::setTeams);
        List<CoachTo> coaches = new ArrayList<>(coachesMap.keySet());
        coaches.sort(Comparator.comparing(CoachTo::getName));
        return coaches;
    }

    public CoachInfoTo get(int id) {
        return coachInfoMapper.toTo(userRepository.getWithCoachTeams(id));
    }

    @Transactional
    public User create(CoachInfoTo to) {
        User coach = coachInfoMapper.toEntity(to);
        assignCoach(coach.getCoachTeams(), coach);
        return userRepository.create(coach);
    }

    @Transactional
    public void update(CoachInfoTo to, int id) {
        User coach = coachInfoMapper.toEntity(to);
        assignCoach(teamRepository.findAllByCoachId(id), null);
        assignCoach(coach.getCoachTeams(), coach);
        userRepository.update(coach, id);
    }

    private void assignCoach(Collection<Team> teams, User coach) {
        teams.forEach(t -> t.setCoach(coach));
    }
}