package space.pirs.modules.training.to;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.BaseTo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
public class ExerciseTo extends BaseTo {

    private BaseTo group;

    @Size(max = 1024)
    @NotNull
    private String description;

    @Size(max = 1024)
    @NotNull
    private String circumstance;

    private Set<BaseTo> skills;

    public ExerciseTo(Integer id, Integer groupId, String description, String circumstance, Set<BaseTo> skills) {
        super(id);
        this.group = new BaseTo(groupId);
        this.description = description;
        this.circumstance = circumstance;
        this.skills = skills;
    }
}
