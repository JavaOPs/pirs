package space.pirs.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import space.pirs.config.AppConfig;

@RestController
@Slf4j
@AllArgsConstructor
public class AppController {

    @GetMapping(value = "/public/counters")
    private AppConfig.AppCounters getAppCounters() {
        return AppConfig.appCounters;
    }
}
