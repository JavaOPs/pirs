package space.pirs.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;
import space.pirs.model.AbstractBaseEntity;

@Transactional(readOnly = true)
public interface AppRepository extends Repository<AbstractBaseEntity, Integer> {
    String SCHOOL = "SCHOOL";

    @Query(value = "SELECT r.ROLE name, count(DISTINCT u.ID) count " +
            "FROM USERS u " +
            "JOIN USER_ROLE r ON r.USER_ID = u.ID " +
            "GROUP BY r.ROLE " +
            "UNION SELECT 'SCHOOL', 1", nativeQuery = true)
    Object[][] getAppCounter();
}