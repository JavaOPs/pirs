package space.pirs.modules.user.web;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.web.util.NestedServletException;
import space.pirs.AbstractControllerTest;
import space.pirs.to.NamedTo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.userHttpBasic;
import static space.pirs.modules.user.UserTestData.*;


public class InfoControllerTest extends AbstractControllerTest{
    private static final String REST_URL = InfoController.REST_URL + "/";

    @Test
    public void testGetTeamsByAdmin() throws Exception{
        mockMvc.perform(get(REST_URL + "teams")
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, NAMED_TEAMS, NamedTo.class));
    }

    @Test
    public void testGetTeamsByCoach() throws Exception{
        mockMvc.perform(get(REST_URL + "teams")
                .with(userHttpBasic(COACH_1)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, COACH_INFO_TO_1.getTeams(), NamedTo.class));
    }

    @Test
    public void testGetCoaches() throws Exception{
        mockMvc.perform(get(REST_URL + "coaches")
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, NAMED_COACHES, NamedTo.class));
    }

    @Test(expected = NestedServletException.class)
    public void testGetTeamsByStudent() throws Exception{
        mockMvc.perform(get(REST_URL + "teams")
                .with(userHttpBasic(USER_STUDENT_1)))
                .andDo(print());
    }

    @Test(expected = NestedServletException.class)
    public void testGetCoachesByCoach() throws Exception{
        mockMvc.perform(get(REST_URL + "coaches")
                .with(userHttpBasic(COACH_1)))
                .andDo(print());
    }
}