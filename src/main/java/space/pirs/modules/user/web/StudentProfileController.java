package space.pirs.modules.user.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.pirs.AuthUser;
import space.pirs.modules.user.service.StudentInfoService;
import space.pirs.modules.user.to.StudentInfoTo;

@RestController
@RequestMapping(value = StudentProfileController.REST_URL, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@AllArgsConstructor
@Slf4j
public class StudentProfileController {
    static final String REST_URL = "/api/student/profile";

    private final StudentInfoService studentInfoService;

    @GetMapping
    private StudentInfoTo getStudentInfo(@AuthenticationPrincipal AuthUser authUser) {
        log.info("getStudentInfo by {}", authUser);
        return studentInfoService.get(authUser.id(), -1);
    }
}
