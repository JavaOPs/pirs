package space.pirs.modules.user.web;

import org.junit.Test;
import org.springframework.http.MediaType;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.user.to.StudentInfoTo;
import space.pirs.modules.user.to.StudentTo;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.getContent;
import static space.pirs.TestUtil.userHttpBasic;
import static space.pirs.modules.user.UserTestData.*;

public class CoachStudentControllerTest extends AbstractControllerTest {
    private static final String REST_URL = CoachStudentController.REST_URL + "/";

    @Test
    public void testGetStudentInfo() throws Exception {
        mockMvc.perform(get(REST_URL + STUDENT_ID_1)
                .with(userHttpBasic(COACH_1)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertMatch(jsonValueMapper.readValue(getContent(result), StudentInfoTo.class), STUDENT_INFO_1, "password"));
    }

    @Test
    public void testGetByTeam() throws Exception {
        mockMvc.perform(get(REST_URL + "by-team")
                .param("teamId", String.valueOf(TEAM_ID_1))
                .with(userHttpBasic(COACH_1)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, List.of(STUDENT_1, STUDENT_2), StudentTo.class));
    }

    @Test
    public void testGetOwns() throws Exception {
        mockMvc.perform(get(REST_URL)
                .with(userHttpBasic(COACH_1)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, List.of(STUDENT_1, STUDENT_2, STUDENT_3), StudentTo.class));
    }

    @Test
    public void testGetOwnsNotAuth() throws Exception {
        mockMvc.perform(get(REST_URL)
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isForbidden())
                .andDo(print());
    }
}