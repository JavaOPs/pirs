package space.pirs.modules.training.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import space.pirs.modules.training.model.Training;
import space.pirs.modules.user.model.User;
import space.pirs.repository.AbstractTimestampRepository;

import java.time.LocalDate;
import java.util.List;

public interface TrainingRepository extends AbstractTimestampRepository<Training> {

    //  https://stackoverflow.com/questions/8339889
    @Query(value = "SELECT DISTINCT t FROM Training t JOIN FETCH t.team " +
            "WHERE (:student MEMBER OF t.students) AND (t.date BETWEEN :startDate AND :endDate) " +
            "ORDER BY t.date")
    List<Training> findAllByStudentAndDateBetween(User student, LocalDate startDate, LocalDate endDate);

    @Query(value = "SELECT DISTINCT t FROM Training t JOIN FETCH t.team " +
            "WHERE (t.team.coach = :coach) AND (t.date BETWEEN :startDate AND :endDate) " +
            "ORDER BY t.date")
    List<Training> findAllByCoachAndDateBetween(User coach, LocalDate startDate, LocalDate endDate);

    default List<Training> getTrainingByStudentAndMonth(User student, int month) {
        LocalDate start = LocalDate.of(LocalDate.now().getYear(), month, 1);
        LocalDate end = start.withDayOfMonth(start.lengthOfMonth());
        return findAllByStudentAndDateBetween(student, start, end);
    }

    default List<Training> getTrainingByCoachAndMonth(User coach, int month) {
        LocalDate start = LocalDate.of(LocalDate.now().getYear(), month, 1);
        LocalDate end = start.withDayOfMonth(start.lengthOfMonth());
        return findAllByCoachAndDateBetween(coach, start, end);
    }

    @Query(value = "SELECT t FROM Training t WHERE t.id=:id AND t.team.coach=:coach")
    Training getByIdAndCoach(int id, User coach);

    @Query("SELECT DISTINCT t FROM Training t " +
            "LEFT JOIN FETCH t.students " +
            "LEFT JOIN FETCH t.team tm " +
            "WHERE t.id = :id AND tm.coach =:coach")
    Training getByIdAndCoachWithInfo(int id, User coach);

    @Modifying
    @Transactional
    @Query("UPDATE Training t SET t.passed=:passed WHERE t.id=:id AND t.team.coach=:coach")
    int passed(int id, boolean passed, User coach);
}