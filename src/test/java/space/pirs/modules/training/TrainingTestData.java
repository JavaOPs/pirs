package space.pirs.modules.training;

import org.springframework.test.web.servlet.ResultMatcher;
import space.pirs.mapper.JsonValueMapper;
import space.pirs.modules.training.to.*;
import space.pirs.to.BaseTo;
import space.pirs.to.NamedTo;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static space.pirs.TestUtil.getContent;
import static space.pirs.modules.user.UserTestData.*;

public class TrainingTestData {
    public static final int EXERCISE_ID_1 = 1;

    public static final int GROUP_ID_1 = 1;
    public static final int GROUP_ID_2 = 2;

    public static final int TRAINING_INFO_ID_1 = 1;

    public static final BaseTo SKILL_1 = new BaseTo(1);
    public static final BaseTo SKILL_2 = new BaseTo(2);

    public static final Set<BaseTo> SKILLS_1 = Set.of(SKILL_1, SKILL_2);

    public static final String DESCRIPTION_1 = "описание1";
    public static final String DESCRIPTION_2 = "описание2";
    public static final String DESCRIPTION_3 = "описание3";

    public static final String CIRCUMSTANCE = "условие1";

    public static final ExerciseTo EXERCISE_1 = new ExerciseTo(EXERCISE_ID_1, GROUP_ID_1, DESCRIPTION_1, CIRCUMSTANCE, SKILLS_1);

    public static final ExerciseTo EXERCISE_EMPTY_SKILLS = new ExerciseTo(EXERCISE_ID_1, GROUP_ID_1, DESCRIPTION_1, null, null);
    public static final ExerciseTo EXERCISE_2 = new ExerciseTo(EXERCISE_ID_1 + 1, GROUP_ID_2, DESCRIPTION_2, null, null);
    public static final ExerciseTo EXERCISE_3 = new ExerciseTo(EXERCISE_ID_1 + 2, GROUP_ID_2, DESCRIPTION_3, null, null);

    public static final List<ExerciseTo> EXERCISE_TO_LIST = List.of(EXERCISE_EMPTY_SKILLS, EXERCISE_2, EXERCISE_3);

    public static final NamedTo NAMED_EXERCISE_1 = new NamedTo(1, "Разминка");
    public static final NamedTo NAMED_EXERCISE_2 = new NamedTo(2, "Основная часть");

    public static final ExerciseGroupTo EXERCISE_GROUP_1 = new ExerciseGroupTo(NAMED_EXERCISE_1, List.of(EXERCISE_1));
    public static final ExerciseGroupTo EXERCISE_GROUP_2 = new ExerciseGroupTo(NAMED_EXERCISE_2, List.of(EXERCISE_2, EXERCISE_3));

    public static final List<ExerciseGroupTo> EXERCISE_GROUPS = List.of(EXERCISE_GROUP_1, EXERCISE_GROUP_2);

    public static final ExerciseShortTo EXERCISE_SHORT_1 = new ExerciseShortTo(1, DESCRIPTION_1);
    public static final ExerciseShortTo EXERCISE_SHORT_2 = new ExerciseShortTo(2, DESCRIPTION_2);
    public static final ExerciseShortTo EXERCISE_SHORT_3 = new ExerciseShortTo(3, DESCRIPTION_3);

    public static final List<ExerciseShortTo> EXERCISE_SHORT_TO_LIST = List.of(EXERCISE_SHORT_1, EXERCISE_SHORT_2, EXERCISE_SHORT_3);

    public static final TrainingItemTo TRAINING_ITEM_1 = new TrainingItemTo(1, 3, 10, 2, 3, EXERCISE_SHORT_1, Set.of(SKILL_1));
    public static final TrainingItemTo TRAINING_ITEM_2 = new TrainingItemTo(2, 3, 10, 2, 3, EXERCISE_SHORT_2, Set.of());
    public static final TrainingItemTo TRAINING_ITEM_3 = new TrainingItemTo(3, 3, 10, 2, 3, EXERCISE_SHORT_3, Set.of(SKILL_2));

    public static final TrainingItemTo TRAINING_ITEM_FOR_SAVE_1 = new TrainingItemTo(null, 3, 10, 2, 3, EXERCISE_SHORT_1, Set.of(SKILL_1));
    public static final TrainingItemTo TRAINING_ITEM_FOR_SAVE_2 = new TrainingItemTo(null, 3, 10, 2, 3, EXERCISE_SHORT_3, Set.of());
    public static final TrainingItemTo TRAINING_ITEM_FOR_SAVE_3 = new TrainingItemTo(null, 3, 10, 2, 3, EXERCISE_SHORT_1, Set.of());
    public static final TrainingItemTo TRAINING_ITEM_FOR_SAVE_4 = new TrainingItemTo(null, 3, 10, 2, 3, EXERCISE_SHORT_2, Set.of(SKILL_1));

    public static final List<TrainingItemTo> TRAINING_ITEMS = List.of(TRAINING_ITEM_1, TRAINING_ITEM_2, TRAINING_ITEM_3);
    public static final List<TrainingItemTo> TRAINING_ITEMS_FOR_SAVE = List.of(TRAINING_ITEM_FOR_SAVE_1, TRAINING_ITEM_FOR_SAVE_2, TRAINING_ITEM_FOR_SAVE_3, TRAINING_ITEM_FOR_SAVE_4);

    public static final TrainingInfoTo TRAINING_INFO_1 = new TrainingInfoTo(
            LocalDate.of(2019, 4, 12),
            LocalTime.of(8, 0), LocalTime.of(10, 0),
            TEAM_NAMED_1, "Тренировка 1, Команда 1", TRAINING_ITEMS, Set.of(MEMBER_1), true);

    public static final TrainingInfoTo UPDATED_TRAINING_INFO_1 = new TrainingInfoTo(
            LocalDate.of(2019, 4, 12),
            LocalTime.of(10, 0), LocalTime.of(11, 0),
            TEAM_NAMED_1, "Тренировка 2, Команда 2", TRAINING_ITEMS, Set.of(MEMBER_1, MEMBER_2, MEMBER_3), true);

    static {
        TRAINING_INFO_1.setId(TRAINING_INFO_ID_1);
        UPDATED_TRAINING_INFO_1.setId(TRAINING_INFO_ID_1);
    }

    public static final TrainingInfoTo CREATED_TRAINING_INFO = new TrainingInfoTo(
            LocalDate.of(2019, 4, 12),
            LocalTime.of(10, 0), LocalTime.of(12, 0),
            TEAM_NAMED_1, "Тренировка 1, Команда 1", TRAINING_ITEMS_FOR_SAVE, Set.of(MEMBER_1), false);

    public static final ScheduleTo COACH_SCHEDULE_1 = new ScheduleTo(
            6, LocalDate.of(2019, 5, 1),
            LocalTime.of(14, 0), LocalTime.of(16, 0),
            "Тренировка 6, Команда 2", TEAM_NAMED_2);

    public static final ScheduleTo COACH_SCHEDULE_2 = new ScheduleTo(
            7, LocalDate.of(2019, 5, 1),
            LocalTime.of(14, 0), LocalTime.of(16, 0),
            "Тренировка 7, Команда 1", TEAM_NAMED_2);

    public static final ScheduleTo COACH_SCHEDULE_3 = new ScheduleTo(
            8, LocalDate.of(2019, 5, 31),
            LocalTime.of(14, 0), LocalTime.of(16, 0),
            "Тренировка 8, Команда 1", TEAM_NAMED_2);

    public static final List<ScheduleTo> COACH_SCHEDULES = List.of(COACH_SCHEDULE_1, COACH_SCHEDULE_2, COACH_SCHEDULE_3);

    public static final ScheduleTo STUDENT_SCHEDULE = new ScheduleTo(
            6, LocalDate.of(2019, 5, 1),
            LocalTime.of(14, 0), LocalTime.of(16, 0), "Тренировка 6, Команда 2", TEAM_NAMED_2);

    public static final List<ScheduleTo> STUDENT_SCHEDULES = List.of(STUDENT_SCHEDULE);

    public static <T> void assertMatch(T actual, T expected) {
        assertThat(actual).isEqualToComparingFieldByFieldRecursively(expected);
    }

    public static <T> void assertMatch(T actual, T expected, String... ignoringFields) {
        assertThat(actual).isEqualToIgnoringGivenFields(expected, ignoringFields);
    }

    public static <T> void assertMatch(Iterable<T> actual, Iterable<T> expected) {
        assertThat(actual).usingRecursiveFieldByFieldElementComparator().isEqualTo(expected);
    }

    public static <T> void assertMatch(Iterable<T> actual, T... expected) {
        assertMatch(actual, List.of(expected));
    }

    public static <T> ResultMatcher contentJson(JsonValueMapper jsonValueMapper, Iterable<T> expected, Class<T> clazz) {
        return result -> assertMatch(jsonValueMapper.readValues(getContent(result), clazz), expected);
    }
}