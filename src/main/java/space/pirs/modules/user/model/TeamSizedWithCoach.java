package space.pirs.modules.user.model;

import java.time.LocalDateTime;

public interface TeamSizedWithCoach {
    Integer getId();

    String getName();

    int getSize();

    int getCoachId();

    String getCoachName();

    String getCoachImgName();

    LocalDateTime getRegistered();
}
