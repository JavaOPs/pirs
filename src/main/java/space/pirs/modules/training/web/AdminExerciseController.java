package space.pirs.modules.training.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.pirs.modules.training.mapper.ExerciseMapper;
import space.pirs.modules.training.mapper.ExerciseSkillMapper;
import space.pirs.modules.training.model.Exercise;
import space.pirs.modules.training.repository.ExerciseRepository;
import space.pirs.modules.training.to.ExerciseTo;
import space.pirs.web.AbstractController;

import java.util.List;

@RestController
@RequestMapping(value = AdminExerciseController.REST_URL)
public class AdminExerciseController extends AbstractController<Exercise, ExerciseTo, ExerciseRepository> {
    public static final String REST_URL = "/api/admin/exercises";

    @Autowired
    private ExerciseMapper exerciseMapper;

    public AdminExerciseController(ExerciseRepository repository, ExerciseSkillMapper mapper) {
        super(repository, mapper, AdminExerciseController.class);
    }

    @GetMapping("/{id}")
    public ExerciseTo get(@PathVariable int id) {
        log.info("get {}", id);
        return mapper.toToWithCheck(repository.getWithSkills(id), id);
    }

    @GetMapping
    public List<ExerciseTo> getAll() {
        log.info("getAll");
        return exerciseMapper.toToList(repository.findAll());
    }
}
