package space.pirs.modules.user.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.pirs.AuthUser;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.UserRepository;

@RestController
@RequestMapping(value = ProfileController.REST_URL, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@AllArgsConstructor
@Slf4j
public class ProfileController {
    static final String REST_URL = "/api/profile";

    private final UserRepository userRepository;

    @GetMapping
    private User get(@AuthenticationPrincipal AuthUser authUser) {
        log.info("get by {}", authUser);
        return userRepository.get(authUser.id());
    }
}
