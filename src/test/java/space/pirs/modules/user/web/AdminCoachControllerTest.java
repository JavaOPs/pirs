package space.pirs.modules.user.web;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.TeamRepository;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.modules.user.to.CoachInfoTo;
import space.pirs.modules.user.to.CoachTo;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.getContent;
import static space.pirs.TestUtil.userHttpBasic;
import static space.pirs.modules.user.UserTestData.*;

public class AdminCoachControllerTest extends AbstractControllerTest {
    private static final String REST_URL = AdminCoachController.REST_URL + "/";

    @Autowired
    UserRepository userRepository;

    @Autowired
    TeamRepository teamRepository;

    @Test
    public void testGetAll() throws Exception {
        mockMvc.perform(get(REST_URL)
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, COACHES_TO, CoachTo.class));
    }

    @Test
    public void testCreate() throws Exception {
        CoachInfoTo created = new CoachInfoTo(NEW_COACH_INFO_TO);
        ResultActions result = mockMvc.perform(post(REST_URL)
                .with(userHttpBasic(ADMIN))
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValueMapper.writeValue(created)))
                .andExpect(status().isOk())
                .andDo(print());
        User coach = new User(NEW_COACH);
        coach.setId(jsonValueMapper.readValue(getContent(result.andReturn()), User.class).getId());
        assertMatch(userRepository.get(coach.getId()), coach, "startpoint", "password");
    }

    @Test
    public void testUpdate() throws Exception {
        CoachInfoTo updated = new CoachInfoTo(EDITED_COACH_INFO_TO_1);
        mockMvc.perform(put(REST_URL + COACH_ID_1)
                .with(userHttpBasic(ADMIN))
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValueMapper.writeValue(updated)))
                .andExpect(status().isOk())
                .andDo(print());
        assertMatch(userRepository.get(COACH_ID_1), COACH_1_EDITED, "startpoint", "password", "imgName");
    }

    @Test
    public void testActivate() throws Exception {
        mockMvc.perform(put(REST_URL + COACH_ID_1 + "/activate")
                .param("activate", "false")
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print());
        assertMatch(userRepository.get(COACH_ID_1).getEndpoint().toLocalDate(), LocalDate.now());
//        assertMatch(teamRepository.findAllByCoachId(COACH_ID_1), List.of());
    }

    @Test
    public void testGet() throws Exception {
        mockMvc.perform(get(REST_URL + COACH_ID_1)
                .with(userHttpBasic(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertMatch(jsonValueMapper.readValue(getContent(result), CoachInfoTo.class), COACH_INFO_TO_1, "password")); //wrong comparing team lists, so I compare them separately
    }
}
