package space.pirs.modules.training.mapper;

import org.mapstruct.Mapper;
import space.pirs.BaseMapper;
import space.pirs.modules.training.model.ExerciseGroup;
import space.pirs.to.NamedTo;

@Mapper(componentModel = "spring")
public abstract class ExerciseGroupMapper implements BaseMapper<ExerciseGroup, NamedTo> {
}
