package space.pirs.modules.user.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import space.pirs.modules.user.model.Team;
import space.pirs.modules.user.model.TeamSizedWithCoach;
import space.pirs.repository.AbstractNamedRepository;

import java.util.List;

@Transactional(readOnly = true)
public interface TeamRepository extends AbstractNamedRepository<Team> {

    @Query("SELECT t FROM Team t LEFT JOIN FETCH t.coach WHERE t.endpoint IS NULL ORDER BY t.name")
    List<Team> findAll();

    @Query("SELECT t FROM Team t JOIN FETCH t.coach WHERE t.coach.id=:coachId AND t.endpoint IS NULL ORDER BY t.name")
    List<Team> findAllByCoachId(int coachId);

    @Query("SELECT t FROM Team t WHERE t.id = :id AND t.coach.id=:coachId AND t.endpoint IS NULL")
    Team getByIdAndCoachId(int id, int coachId);

    @Query(value = "SELECT t.ID as id, t.NAME as NAME, coach.ID as coachId, coach.NAME as coachName, coach.IMG_NAME as coachImgName, coach.STARTPOINT as registered, count(DISTINCT u.ID) as size\n" +
            "FROM USERS coach\n" +
            "         JOIN USER_ROLE ur ON coach.ID = ur.USER_ID AND ur.ROLE='ROLE_COACH'\n" +
            "         LEFT JOIN TEAM t on t.COACH_ID = coach.ID\n" +
            "         LEFT JOIN USERS u on t.ID = u.TEAM_ID AND u.ENDPOINT IS NULL\n" +
            "GROUP BY t.ID", nativeQuery = true)
    List<TeamSizedWithCoach> getAllWithSize();

    @Query("SELECT t FROM Team t LEFT JOIN FETCH t.students LEFT JOIN FETCH t.coach WHERE t.id = :id AND t.endpoint IS NULL")
    Team getByIdWithStudents(int id);
}