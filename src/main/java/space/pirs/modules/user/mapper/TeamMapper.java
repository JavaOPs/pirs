package space.pirs.modules.user.mapper;

import org.mapstruct.Mapper;
import space.pirs.BaseMapper;
import space.pirs.modules.user.model.Team;
import space.pirs.modules.user.to.TeamTo;

@Mapper(componentModel = "spring")
public abstract class TeamMapper implements BaseMapper<Team, TeamTo> {
}