package space.pirs.modules.user.mapper;

import org.mapstruct.Mapper;
import space.pirs.BaseMapper;
import space.pirs.modules.user.model.User;
import space.pirs.to.NamedTo;

@Mapper(componentModel = "spring")
public abstract class UserNamedMapper implements BaseMapper<User, NamedTo> {
}
