package space.pirs.modules.training.repository;

import org.springframework.data.jpa.repository.Query;
import space.pirs.modules.training.model.Exercise;
import space.pirs.repository.AbstractRepository;

public interface ExerciseRepository extends AbstractRepository<Exercise> {

    @Query("SELECT e FROM Exercise e LEFT JOIN FETCH e.skills WHERE e.id=:id")
    Exercise getWithSkills(int id);
}
