package space.pirs.modules.training.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.pirs.modules.training.mapper.ExerciseGroupMapper;
import space.pirs.modules.training.mapper.ExerciseGroupTreeMapper;
import space.pirs.modules.training.model.ExerciseGroup;
import space.pirs.modules.training.repository.ExerciseGroupRepository;
import space.pirs.modules.training.to.ExerciseGroupTo;
import space.pirs.to.NamedTo;
import space.pirs.web.AbstractController;

import java.util.List;

@RestController
@RequestMapping(value = AdminExerciseGroupController.REST_URL)
public class AdminExerciseGroupController extends AbstractController<ExerciseGroup, NamedTo, ExerciseGroupRepository> {
    public static final String REST_URL = "/api/admin/exercisesGroup";

    @Autowired
    private ExerciseGroupTreeMapper treeMapper;

    public AdminExerciseGroupController(ExerciseGroupRepository repository, ExerciseGroupMapper mapper) {
        super(repository, mapper, AdminExerciseGroupController.class);
    }

    @GetMapping("/tree")
    public List<ExerciseGroupTo> getTree() {
        log.info("getAllWithExercises");
        List<ExerciseGroup> groups = repository.getAllWithExercises();
        return treeMapper.toToList(groups);
    }
}
