package space.pirs.modules.training.mapper;

import org.mapstruct.Mapper;
import space.pirs.BaseMapper;
import space.pirs.modules.training.model.Exercise;
import space.pirs.modules.training.to.ExerciseShortTo;

@Mapper(componentModel = "spring")
public abstract class ExerciseShortMapper implements BaseMapper<Exercise, ExerciseShortTo> {
}
