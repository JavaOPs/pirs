package space.pirs.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class NamedImgTo extends NamedTo {
    public NamedImgTo(Integer id, String name, String imgName) {
        super(id, name);
        this.imgName = imgName;
    }

    @Size(max = 128)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    protected String imgName;
}
