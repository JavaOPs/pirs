package space.pirs.modules.user.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import space.pirs.BaseMapper;
import space.pirs.modules.user.model.Role;
import space.pirs.modules.user.model.Team;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.TeamRepository;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.modules.user.to.CoachInfoTo;
import space.pirs.to.NamedTo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class CoachInfoMapper implements BaseMapper<User, CoachInfoTo> {
    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected CoachTeamMapper coachTeamMapper;

    @Mappings({
            @Mapping(target = "teams", source = "coachTeams"),
    })
    public abstract CoachInfoTo toTo(User coach);

    @Override
    public User toEntity(CoachInfoTo to) {
        User coach;
        if (to.isNew()) {
            coach = new User(null, to.getEmail(), to.getName(), to.getPassword(), Set.of(Role.ROLE_COACH), null);
        } else {
            coach = userRepository.getCoach(to.getId());
            coach.setEmail(to.getEmail());
            coach.setName(to.getName());
        }
        List<Team> teamList = coachTeamMapper.toEntityList(to.getTeams());
        coach.setCoachTeams(new HashSet<>(teamList));
        return coach;
    }

    @Mapper(componentModel = "spring")
    public abstract static class CoachTeamMapper implements BaseMapper<Team, NamedTo> {
        @Autowired
        private TeamRepository teamRepository;

        public Team toEntity(NamedTo teamTo) {
            return teamTo == null ? null : teamRepository.getOne(teamTo.id());
        }
    }
}
