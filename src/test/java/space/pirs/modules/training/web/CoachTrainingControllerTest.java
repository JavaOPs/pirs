package space.pirs.modules.training.web;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.util.NestedServletException;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.training.mapper.TrainingInfoMapper;
import space.pirs.modules.training.model.Training;
import space.pirs.modules.training.repository.TrainingRepository;
import space.pirs.modules.training.to.ExerciseShortTo;
import space.pirs.modules.training.to.TrainingInfoTo;

import static junit.framework.TestCase.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.*;
import static space.pirs.modules.training.TrainingTestData.*;
import static space.pirs.modules.user.UserTestData.COACH_1;

public class CoachTrainingControllerTest extends AbstractControllerTest {
    private static final String REST_URL = CoachTrainingController.REST_URL + "/";

    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private TrainingInfoMapper trainingInfoMapper;

    @Test
    public void testGet() throws Exception {
        mockMvc.perform(get(REST_URL + TRAINING_INFO_ID_1)
                .with(userAuth(COACH_1)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertMatch(jsonValueMapper.readValue(getContent(result), TrainingInfoTo.class), TRAINING_INFO_1));
    }

    @Test(expected = NestedServletException.class)
    public void testGetNotFound() throws Exception {
        mockMvc.perform(get(REST_URL + "0")
                .with(userAuth(COACH_1)))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void testGetNotAuth() throws Exception {
        mockMvc.perform(get(REST_URL))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testCreate() throws Exception {
        TrainingInfoTo created = CREATED_TRAINING_INFO.toBuilder().build();

        ResultActions action = mockMvc.perform(post(REST_URL)
                .with(userHttpBasic(COACH_1))
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValueMapper.writeValue(created)));

        Training returned = jsonValueMapper.readValue(getContent(action.andReturn()), Training.class);
        created.setId(returned.getId());
        for(int i = 0; i < returned.getItems().size(); i++) {
            int id = returned.getItems().get(i).getId();
            created.getItems().get(i).setId(id);
        }
        assertMatch(trainingInfoMapper.toTo(returned), created, "exercise");
    }

    @Test
    public void testUpdate() throws Exception {
        TrainingInfoTo updated = UPDATED_TRAINING_INFO_1.toBuilder().build();

        ResultActions action = mockMvc.perform(put(REST_URL + TRAINING_INFO_ID_1)
                .with(userHttpBasic(COACH_1))
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValueMapper.writeValue(updated)))
                .andExpect(status().isOk());

        Training returned = jsonValueMapper.readValue(getContent(action.andReturn()), Training.class);
        assertMatch(trainingInfoMapper.toTo(returned), UPDATED_TRAINING_INFO_1, "exercise");
    }

    @Test
    public void testActivate() throws Exception {
        mockMvc.perform(put(REST_URL + TRAINING_INFO_ID_1 + "/activate")
                .param("activate", "false")
                .with(userHttpBasic(COACH_1)))
                .andExpect(status().isOk());

        assertNotNull(trainingRepository.getByIdAndCoach(TRAINING_INFO_ID_1, COACH_1).getEndTime());
    }

    @Test
    public void getExercises() throws Exception {
        mockMvc.perform(get(REST_URL + "exercises")
                .with(userAuth(COACH_1)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, EXERCISE_SHORT_TO_LIST, ExerciseShortTo.class));
    }
}