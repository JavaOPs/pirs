package space.pirs.modules.training.to;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.BaseTo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Setter
@Getter
@NoArgsConstructor
public class ExerciseShortTo extends BaseTo {
    @Size(max = 1024)
    @NotNull
    private String description;

    public ExerciseShortTo(Integer id, String description) {
        super(id);
        this.description = description;
    }
}
