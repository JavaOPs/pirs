package space.pirs.modules.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import space.pirs.modules.user.model.StudentDetails;

import java.util.List;

@Transactional(readOnly = true)
public interface StudentDetailsRepository extends JpaRepository<StudentDetails, Integer> {

    @Query("SELECT d FROM StudentDetails d WHERE d.student.id=:id")
    StudentDetails findByUserId(int id);

    @Query("SELECT d FROM StudentDetails d JOIN FETCH d.student u LEFT JOIN FETCH u.team t JOIN FETCH t.coach WHERE u.id=:id")
    StudentDetails fetchWithUser(int id);
}