package space.pirs.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.TeamRepository;
import space.pirs.modules.user.repository.UserRepository;

import static space.pirs.util.ValidationUtil.checkFoundById;
import static space.pirs.util.ValidationUtil.checkFoundByIdAndCoach;

@Service
@AllArgsConstructor
public class UserValidationService {
    private final UserRepository userRepository;
    private final TeamRepository teamRepository;

    public void checkTeamBelongCoach(int teamId, User coach) {
        checkFoundByIdAndCoach(teamRepository.getByIdAndCoachId(teamId, coach.getId()), teamId, coach);
    }

    public User checkStudentBelongCoach(int studentId, int coachId) {
        User student = checkFoundById(userRepository.getWithTeam(studentId), studentId);
        if (student.getTeam() == null || student.getTeam().getCoach() == null || coachId != student.getTeam().getCoach().id()) {
            throw new IllegalArgumentException("Student " + student + " is not belong to coach " + coachId);
        }
        return student;
    }
}
