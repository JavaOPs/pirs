package space.pirs.modules.training.web;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.web.util.NestedServletException;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.training.to.ScheduleTo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.userAuth;
import static space.pirs.modules.training.TrainingTestData.*;
import static space.pirs.modules.user.UserTestData.COACH_1;
import static space.pirs.modules.user.UserTestData.COACH_2;

public class CoachScheduleControllerTest extends AbstractControllerTest {
    private static final String REST_URL = CoachScheduleController.REST_URL + "/";

    @Test
    public void testGetTrainingByMonth() throws Exception {
        mockMvc.perform(get(REST_URL)
                .param("month", "5")
                .with(userAuth(COACH_1)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, COACH_SCHEDULES, ScheduleTo.class));
    }

    @Test
    public void testGetTrainingByMonthByStudent() throws Exception {
        mockMvc.perform(get(REST_URL)
                .param("month", "5")
                .param("studentId", "3")
                .with(userAuth(COACH_1)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, STUDENT_SCHEDULES, ScheduleTo.class));
    }

    @Test(expected = NestedServletException.class)
    public void testWrongCoach() throws Exception {
        mockMvc.perform(get(REST_URL)
                .param("month", "5")
                .param("studentId", "3")
                .with(userAuth(COACH_2)))
                .andExpect(status().is5xxServerError());
    }

}