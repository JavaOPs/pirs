package space.pirs.modules.training.mapper;

import org.mapstruct.Mapper;
import space.pirs.BaseMapper;
import space.pirs.modules.training.model.Training;
import space.pirs.modules.training.model.TrainingItem;
import space.pirs.modules.training.to.TrainingInfoTo;
import space.pirs.modules.training.to.TrainingItemTo;

@Mapper(componentModel = "spring", uses = TrainingInfoMapper.TrainingItemMapper.class)
public abstract class TrainingInfoMapper implements BaseMapper<Training, TrainingInfoTo> {

    @Mapper(componentModel = "spring")
    public abstract static class TrainingItemMapper implements BaseMapper<TrainingItem, TrainingItemTo> {
    }
}
