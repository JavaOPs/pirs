package space.pirs.web;

import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import space.pirs.BaseMapper;
import space.pirs.model.AbstractTimestampEntry;
import space.pirs.repository.AbstractTimestampRepository;
import space.pirs.to.BaseTo;

import javax.validation.Valid;
import java.util.List;

public abstract class AbstractTimestampController<E extends AbstractTimestampEntry, T extends BaseTo, R extends AbstractTimestampRepository<E>> extends BaseWebHandler<E, T, R> {

    public AbstractTimestampController(R repository, BaseMapper<E, T> mapper, Class controllerClass) {
        super(repository, mapper, controllerClass);
    }

    @GetMapping("/{id}")
    public T get(@PathVariable int id) {
        return super.get(id);
    }

    @GetMapping
    public List<T> getAll() {
        log.info("getAll");
        return mapper.toToList(repository.findAllByEndpointIsNull());
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public E create(@RequestBody @Valid T to) {
        log.info("create {}", to);
        return repository.createWithTimestamp(mapper.toEntity(to));
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public void update(@RequestBody @Valid T to, @PathVariable int id) {
        log.info("update {}, {}", to, id);
        repository.updateWithTimestamp(mapper.toEntity(to), id);
    }

    @PutMapping("/{id}/activate")
    public void activate(@PathVariable int id, @RequestParam boolean activate) {
        log.info("activate {}, {}", id, activate);
        repository.activate(id, activate);
    }
}