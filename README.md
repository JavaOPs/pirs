## Система мониторинга показателей спортсменов
- [ПЕРВОЕ ЗАНЯТИЕ ПРОЕКТА BOOTJAVA](http://javaops.ru/view/bootjava/lesson01)

- User: admin
- DB:   jdbc:h2:tcp://localhost:9092/mem:pirs

## Swagger 2 API documentation [доступна при развернутом приложении](http://localhost:8080/swagger-ui.html)
## [Распределение задач бэкэнда](https://docs.google.com/spreadsheets/d/1yzxS_vrkMlpv1csfeYcogvruRoxJZHzD7czMBR7-ZZw)
## Примеры запросов в IDEA Tools->Http Client->Http Request History
```
GET http://localhost:8080/api/admin/students/3
Authorization: Basic admin@javaops.ru admin
Content-Type: application/json
```

```
GET http://localhost:8080/api/admin/users
Authorization: Basic admin@javaops.ru admin
Content-Type: application/json
```

```
GET http://localhost:8080/api/coach/students/3
Authorization: Basic coach@gmail.com coach
Content-Type: application/json
```

```
POST http://localhost:8080/api/admin/users
Authorization: Basic admin@javaops.ru admin
Content-Type: application/json

{
  "email": "coach1@gmail.com",
  "name": "Семен Семенович",
  "password": "12345",
  "roles": [
    "ROLE_COACH"
  ]
}
```
#Student

GET http://localhost:8080/api/admin/students/
Authorization: Basic admin@javaops.ru admin
Content-Type: application/json

#Team
### get All teams TO

GET http://localhost:8080/api/admin/teams/to
Authorization: Basic admin@javaops.ru admin
Content-Type: application/json

### gat teams by coach

GET http://localhost:8080/api/admin/teams/by-coach/1
Authorization: Basic admin@javaops.ru admin
Content-Type: application/json

### get players by team

GET http://localhost:8080/api/admin/teams/1
Authorization: Basic admin@javaops.ru admin
Content-Type: application/json

### get team with members

GET http://localhost:8080/api/admin/teams/1
Authorization: Basic admin@javaops.ru admin
Content-Type: application/json


### update team with members

PUT http://localhost:8080/api/admin/teams/1
Authorization: Basic admin@javaops.ru admin
Content-Type: application/json

{
  "id": 1,
  "name": "Команда 1",
  "coach": {
    "id": 1,
    "name": "Иванов Иван Иванович"
  },
  "students": [
    {
      "id": 3,
      "name": "Константинопольский Константин Константинович"
    }
  ]
}

### create team with members

POST http://localhost:8080/api/admin/teams
Authorization: Basic admin@javaops.ru admin
Content-Type: application/json

{
  "name": "Команда 4",
  "coach": {
    "id": 6,
    "name": "Тренер без команд"
  },
  "students": [
    {
      "id": 3,
      "name": "Константинопольский Константин Константинович"
    },
    {
      "id": 4,
      "name": "Петров Петр Петрович"
    }
  ]
}