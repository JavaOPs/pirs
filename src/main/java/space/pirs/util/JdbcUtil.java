package space.pirs.util;

import space.pirs.exception.ApplicationException;

import java.sql.Array;
import java.sql.SQLException;

public class JdbcUtil {
    public static Object[] EMPTY_ARRAY = new Object[0];

    public static Object[] getArray(Array array) {
        if (array == null) {
            return EMPTY_ARRAY;
        }
        try {
            Object[] objects = (Object[]) array.getArray();
            array.free();
            return objects;
        } catch (SQLException e) {
            throw new ApplicationException(e);
        }
    }
}