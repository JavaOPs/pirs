package space.pirs.to;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.HasName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class NamedTo extends BaseTo implements HasName {
    public NamedTo(Integer id, String name) {
        super(id);
        this.name = name;
    }

    @NotNull
    @NotBlank
    protected String name;

    @Override
    public String toString() {
        return super.toString() + '(' + name + ')';
    }
}
