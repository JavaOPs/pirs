----------------------  USERS -------------------------------------

INSERT INTO USERS (EMAIL, NAME, PASSWORD, IMG_NAME)
VALUES ('coach@gmail.com', 'Иванов Иван Иванович', '{noop}coach', 't1.jpg'),
       ('admin@javaops.ru', 'Ivan Petrov', '{noop}admin', NULL);

INSERT INTO TEAM (ID, NAME, COACH_ID)
VALUES (1, 'Команда 1', 1),
       (2, 'Команда 2', 1);

INSERT INTO USERS (EMAIL, NAME, PASSWORD, TEAM_ID, IMG_NAME)
VALUES ('student@mail.ru', 'Константинопольский Константин Константинович', '{noop}student', 1, 'foot1.jpg'),
       ('student1@mail.ru', 'Петров Петр Петрович', '{noop}student1', 1, 'xw_1547274.jpg'),
       ('test@java.ru', 'Тест Тестер Тестович', '{noop}test', 2, 'foot3.jpg'),
       ('coach2@gmail.com', 'Тренер без команд', '{noop}coach2', NULL, 't2.jpg'),
       ('coach3@gmail.com', 'Еще один тренер', '{noop}coach3', NULL, 't3.jpg');

INSERT INTO USER_ROLE (ROLE, USER_ID)
VALUES ('ROLE_COACH', 1),
       ('ROLE_ADMIN', 2),
       ('ROLE_STUDENT', 3),
       ('ROLE_STUDENT', 4),
       ('ROLE_STUDENT', 5),
       ('ROLE_COACH', 6),
       ('ROLE_COACH', 7);

INSERT INTO TEAM (ID, NAME, COACH_ID)
VALUES (3, 'Команда 3', 7);

INSERT INTO STUDENT_DETAILS (ID, BIRTHDAY, START_CAREER, START_TEAM_ID, START_EDUCATION_DATE, PHONE, ADDRESS, CATEGORY,
                             HEIGHT, WEIGHT, FOOT_SIZE, CHEST_GIRTH, WAIST_GIRTH, HIP_GIRTH, STUDENT_ID)
VALUES (1, '1996-05-22', 2014, 1, '2008-11-11',
        '+7 123 123-45-67',
        'г.Пермь, ул.Футболистов, д.4', 'КМС', 182, 65, 42, 96, 72, 85, 3);

INSERT INTO STUDENT_DETAILS (ID, BIRTHDAY, START_CAREER, START_TEAM_ID, START_EDUCATION_DATE, PHONE, ADDRESS, CATEGORY,
                             HEIGHT, WEIGHT, FOOT_SIZE, CHEST_GIRTH, WAIST_GIRTH, HIP_GIRTH, STUDENT_ID)
VALUES (2, '1996-05-22', 2014, 2, '2008-11-11',
        '+7 123 123-45-67',
        'г.Пермь, ул.Чемпионов, д.4', 'МС', 182, 65, 42, 96, 72, 85, 4);

----------------------  HEALTH -------------------------------------
INSERT INTO STUDENT_HEALTH (STUDENT_ID)
values (3);

INSERT INTO INSURANCE_POLICY (POLICY_NUMBER, START_DATE, END_DATE, POLICY_TYPE, STUDENT_HEALTH_ID)
values ('10011977', '2019-01-01', '2020-01-01', 'OMS', 1),
       ('10123011977', '2019-01-01', '2021-01-01', 'DMS', 1);


INSERT INTO HEALTH_EVENT (START_DATE, END_DATE, DIAGNOSIS, COMMENT, HEALTH_STATE, STUDENT_HEALTH_ID)
VALUES ('2019-02-22', '2019-02-22', 'Травма колена', 'Все сложно', 'INJURY', 1),
       ('2019-02-23', '2019-03-21', 'Травма колена', 'Зашиваем. Накладываем гипс', 'TREATMENT', 1),
       ('2019-03-22', '2019-04-30', 'Травма колена', 'Понемногу расхаживаемся', 'REHABILITATION', 1),
       ('2019-04-13', '2019-04-13', 'Травма плеча', 'Все сложно опять', 'INJURY', 1),
       ('2019-04-13', '2019-04-20', 'Травма плеча', 'Опять зашиваем и накладываем гипс', 'TREATMENT', 1),
       ('2019-04-21', '2019-05-20', 'Травма плеча', 'Опять понемногу расхаживаемся', 'REHABILITATION', 1);

------------------------Exercise--------------------------------------
INSERT INTO EXERCISE_GROUP(ID, NAME)
VALUES (1, 'Разминка'),
       (2, 'Основная часть');

INSERT INTO EXERCISE (ID, CIRCUMSTANCE, DESCRIPTION, GROUP_ID)
VALUES (1, 'условие1', 'описание1', 1),
       (2, 'условие2', 'описание2', 2),
       (3, 'условие3', 'описание3', 2);
-----------------------Skills----------------------------------------
INSERT INTO SKILL(ID, NAME, INCREMENTATION, TRAINING_PER_WEEK)
VALUES (1, 'Остановка мяча', 1, 5),
       (2, 'Пас короткий', 1, 4),
       (3, 'Ввод мяча ногой с рук', 1, 6);

INSERT INTO EXERCISE_SKILL(EXERCISE_ID, SKILL_ID)
VALUES (1, 1),
       (1, 2),
       (2, 3),
       (3, 2),
       (3, 3);

----------------------  TRAINING -------------------------------------
INSERT INTO TRAINING (ID, COMMENT, DATE, END_TIME, PASSED, START_TIME, TEAM_ID)
VALUES (1, 'Тренировка 1, Команда 1', '2019-04-12', '10:00:00', TRUE, '08:00:00', 1),
       (2, 'Тренировка 2, Команда 1', '2019-04-15', '18:00:00', FALSE, '16:00:00', 1),
       (3, 'Тренировка 3, Команда 2', '2019-04-12', '16:00:00', TRUE, '14:00:00', 2),
       (4, 'Тренировка 4, Команда 2', '2019-04-13', '10:00:00', FALSE, '08:00:00', 2),
       (5, 'Тренировка 5, Команда 2', '2019-04-15', '16:00:00', FALSE, '14:00:00', 2),
       (6, 'Тренировка 6, Команда 2', '2019-05-01', '16:00:00', FALSE, '14:00:00', 2),
       (7, 'Тренировка 7, Команда 1', '2019-05-01', '16:00:00', FALSE, '14:00:00', 2),
       (8, 'Тренировка 8, Команда 1', '2019-05-31', '16:00:00', FALSE, '14:00:00', 2);

INSERT INTO TRAINING_ITEM (ID, DURATION, PAUSE, ORDINAL, REPEAT, SERIES, EXERCISE_ID, TRAINING_ID)
VALUES (1, 2, 3, 0, 10, 3, 1, 1),
       (2, 2, 3, 1, 10, 3, 2, 1),
       (3, 2, 3, 2, 10, 3, 3, 1),
       (4, 2, 3, 0, 10, 3, 1, 2);

INSERT INTO TRAINING_STUDENT (TRAINING_ID, STUDENT_ID)
VALUES (1, 3),
       (2, 3),
       (3, 3),
       (3, 4),
       (3, 5),
       (4, 4),
       (5, 4),
       (6, 3),
       (7, 4),
       (8, 4);

INSERT INTO TRAINING_ITEM_SKILL (TRAINING_ITEM_ID, SKILL_ID)
VALUES (1, 1),
       (3, 2);