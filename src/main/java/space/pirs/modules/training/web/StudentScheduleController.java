package space.pirs.modules.training.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import space.pirs.AuthUser;
import space.pirs.modules.training.mapper.ScheduleMapper;
import space.pirs.modules.training.repository.TrainingRepository;
import space.pirs.modules.training.to.ScheduleTo;

import java.util.List;

@RestController
@RequestMapping(value = StudentScheduleController.REST_URL)
@Slf4j
@AllArgsConstructor
public class StudentScheduleController {
    public static final String REST_URL = "/api/student/schedule";

    private final ScheduleMapper mapper;
    private final TrainingRepository repository;

    @GetMapping
    public List<ScheduleTo> getTrainingByMonth(@RequestParam int month, @AuthenticationPrincipal AuthUser student) {
        log.info("getTrainingByMonth {} by {}", month, student);
        return mapper.toToList(repository.getTrainingByStudentAndMonth(student.getUser(), month));
    }
}
