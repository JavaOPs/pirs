package space.pirs.modules.user.to;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.NamedTo;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class StudentWithCoachTo extends StudentTo {
    private NamedTo coach;
    private LocalDate registered;

    public StudentWithCoachTo(String name, String imgName, Integer teamId, String teamName, Integer coachId, String coachName) {
        super(name, imgName, teamId, teamName);
        this.coach = new NamedTo(coachId, coachName);
    }
}
