package space.pirs.modules.user.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.pirs.AuthUser;
import space.pirs.exception.ApplicationException;
import space.pirs.modules.user.mapper.TeamNamedMapper;
import space.pirs.modules.user.mapper.UserNamedMapper;
import space.pirs.modules.user.model.Role;
import space.pirs.modules.user.repository.TeamRepository;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.to.NamedTo;

import java.util.List;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = InfoController.REST_URL)
public class InfoController {
    static final String REST_URL = "/api/info";

    private final TeamRepository teamRepository;
    private final UserRepository userRepository;
    private final TeamNamedMapper teamMapper;
    private final UserNamedMapper userMapper;

    @GetMapping("/teams")
    public List<NamedTo> getTeams(@AuthenticationPrincipal AuthUser authUser) {
        log.info("getTeams");
        List<NamedTo> result;
        if (authUser.hasRole(Role.ROLE_ADMIN)) {
            result = teamMapper.toToList(teamRepository.findAll());
        } else if (authUser.hasRole(Role.ROLE_COACH)) {
            result = teamMapper.toToList(teamRepository.findAllByCoachId(authUser.id()));
        } else {
            throw new ApplicationException("Forbidden for " + Role.ROLE_STUDENT);
        }
        return result;
    }

    @GetMapping("/coaches")
    public List<NamedTo> getCoaches(@AuthenticationPrincipal AuthUser authUser) {
        log.info("getCoaches");
        authUser.checkRole(Role.ROLE_ADMIN);
        return userMapper.toToList(userRepository.findAllByRole(Role.ROLE_COACH));
    }
}