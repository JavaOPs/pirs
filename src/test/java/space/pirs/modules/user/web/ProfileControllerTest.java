package space.pirs.modules.user.web;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.user.model.User;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.getContent;
import static space.pirs.TestUtil.userHttpBasic;
import static space.pirs.modules.user.UserTestData.COACH_2;
import static space.pirs.modules.user.UserTestData.assertMatch;

public class ProfileControllerTest extends AbstractControllerTest {
    private static final String REST_URL = ProfileController.REST_URL + "/";

    @Test
    public void testGet() throws Exception {
        ResultActions result = mockMvc.perform(get(REST_URL)
                .with(userHttpBasic(COACH_2)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
        User returned = jsonValueMapper.readValue(getContent(result.andReturn()), User.class);
        assertMatch(returned, COACH_2, "startpoint", "password");
    }

    @Test
    public void testNotAuth() throws Exception {
        mockMvc.perform(get(REST_URL))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}