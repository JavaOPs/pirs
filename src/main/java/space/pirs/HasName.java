package space.pirs;

public interface HasName extends HasId {
    String getName();

    void setName(String name);
}
