package space.pirs.modules.user.mapper;

import org.mapstruct.Mapper;
import space.pirs.BaseMapper;
import space.pirs.modules.user.model.Team;
import space.pirs.to.NamedTo;

@Mapper(componentModel = "spring")
public abstract class TeamNamedMapper implements BaseMapper<Team, NamedTo> {
}
