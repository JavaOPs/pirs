package space.pirs.modules.user.to;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.NamedImgTo;
import space.pirs.to.NamedTo;

@Getter
@Setter
@NoArgsConstructor
public class StudentTo extends NamedImgTo {
    public StudentTo(String name, String imgName, Integer teamId, String teamName) {
        super(null, name, imgName);
        this.team = new NamedTo(teamId, teamName);
    }

    private NamedTo team;
}