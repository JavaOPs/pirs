package space.pirs.modules.skill.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import space.pirs.model.AbstractRefEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name = "skill")
@Getter
public class Skill extends AbstractRefEntity {

    @Column(name = "training_per_week")
    private int trainingPerWeek;

    @Column(name = "incrementation")
    private int incrementation;
}
