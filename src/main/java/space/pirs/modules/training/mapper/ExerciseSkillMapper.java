package space.pirs.modules.training.mapper;

import org.mapstruct.Mapper;
import space.pirs.BaseMapper;
import space.pirs.modules.training.model.Exercise;
import space.pirs.modules.training.to.ExerciseTo;

@Mapper(componentModel = "spring")
public abstract class ExerciseSkillMapper implements BaseMapper<Exercise, ExerciseTo> {
}
