package space.pirs.modules.user;

import org.springframework.test.web.servlet.ResultMatcher;
import space.pirs.mapper.JsonValueMapper;
import space.pirs.modules.user.model.Role;
import space.pirs.modules.user.model.Team;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.to.*;
import space.pirs.to.NamedTo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static space.pirs.TestUtil.getContent;

public class UserTestData {
    public static final int STUDENT_ID_1 = 3;
    public static final int STUDENT_ID_2 = 4;
    public static final int STUDENT_ID_3 = 5;
    public static final String STUDENT_NAME_1 = "Константинопольский Константин Константинович";
    public static final String STUDENT_NAME_2 = "Петров Петр Петрович";
    public static final String STUDENT_NAME_3 = "Тест Тестер Тестович";
    public static final int TEAM_ID_1 = 1;
    public static final int TEAM_ID_2 = 2;
    public static final int TEAM_ID_3 = 3;
    public static final String TEAM_NAME_1 = "Команда 1";
    public static final String TEAM_NAME_2 = "Команда 2";
    public static final String TEAM_NAME_3 = "Команда 3";

    public static final StudentTo STUDENT_1 = new StudentTo(STUDENT_NAME_1, null, TEAM_ID_1, TEAM_NAME_1);
    public static final StudentTo STUDENT_2 = new StudentTo(STUDENT_NAME_2, null, TEAM_ID_1, TEAM_NAME_1);
    public static final StudentTo STUDENT_3 = new StudentTo(STUDENT_NAME_3, null, TEAM_ID_2, TEAM_NAME_2);

    static {
        STUDENT_1.setId(STUDENT_ID_1);
        STUDENT_2.setId(STUDENT_ID_2);
        STUDENT_3.setId(STUDENT_ID_3);
    }

    public static final List<StudentTo> STUDENTS = List.of(STUDENT_1, STUDENT_2, STUDENT_3);

    public static int ADMIN_ID = 2;
    public static User ADMIN = new User(ADMIN_ID, "admin@javaops.ru", "Ivan Petrov", "admin", Set.of(Role.ROLE_ADMIN), null);

    public static final int COACH_ID_1 = 1;
    public static final int COACH_ID_2 = 6;
    public static final int COACH_ID_3 = 7;
    public static final String COACH_NAME_1 = "Иванов Иван Иванович";
    public static final String COACH_NAME_2 = "Тренер без команд";
    public static final String COACH_NAME_3 = "Еще один тренер";

    public static final User COACH_1 = new User(COACH_ID_1, "coach@gmail.com", COACH_NAME_1, "coach", Set.of(Role.ROLE_COACH), null);
    public static final User COACH_2 = new User(COACH_ID_2, "coach2@gmail.com", COACH_NAME_2, "coach2", Set.of(Role.ROLE_COACH), null);
    public static final User COACH_3 = new User(COACH_ID_2, "coach3@gmail.com", COACH_NAME_3, "coach3", Set.of(Role.ROLE_COACH), null);

    static {
        COACH_1.setImgName("t1.jpg");
        COACH_2.setImgName("t2.jpg");
        COACH_3.setImgName("t3.jpg");
    }

    public static final User NEW_COACH = new User(null, "coach_new@gmail.com", "Новый тренер", "coachnew", Set.of(Role.ROLE_COACH), null);

    private static final String STUDENT_1_EMAIL = "student@mail.ru";
    private static final String STUDENT_1_PSW = "student";
    private static final String STUDENT_1_NAME = "Константинопольский Константин Константинович";

    public static final NamedTo COACH_NAMED_1 = new NamedTo(COACH_ID_1, COACH_NAME_1);
    public static final NamedTo COACH_NAMED_2 = new NamedTo(COACH_ID_2, COACH_NAME_2);
    public static final NamedTo COACH_NAMED_3 = new NamedTo(COACH_ID_3, COACH_NAME_3);
    public static final NamedTo TEAM_NAMED_1 = new NamedTo(TEAM_ID_1, TEAM_NAME_1);
    public static final NamedTo TEAM_NAMED_2 = new NamedTo(TEAM_ID_2, TEAM_NAME_2);
    public static final NamedTo TEAM_NAMED_3 = new NamedTo(TEAM_ID_3, TEAM_NAME_3);

    public static final StudentWithCoachTo STUDENT_WITH_COACH_1 = new StudentWithCoachTo(STUDENT_NAME_1, null, TEAM_ID_1, TEAM_NAME_1, COACH_ID_1, COACH_NAME_1);
    public static final StudentWithCoachTo STUDENT_WITH_COACH_2 = new StudentWithCoachTo(STUDENT_NAME_2, null, TEAM_ID_1, TEAM_NAME_1, COACH_ID_1, COACH_NAME_1);
    public static final StudentWithCoachTo STUDENT_WITH_COACH_3 = new StudentWithCoachTo(STUDENT_NAME_3, null, TEAM_ID_2, TEAM_NAME_2, COACH_ID_1, COACH_NAME_1);

    static {
        STUDENT_WITH_COACH_1.setId(STUDENT_ID_1);
        STUDENT_WITH_COACH_2.setId(STUDENT_ID_2);
        STUDENT_WITH_COACH_3.setId(STUDENT_ID_3);
    }

    public static final List<StudentWithCoachTo> STUDENTS_WITH_COACH = List.of(STUDENT_WITH_COACH_1, STUDENT_WITH_COACH_2, STUDENT_WITH_COACH_3);

    public static final StudentInfoTo STUDENT_INFO_1 = new StudentInfoTo(STUDENT_1_NAME,
            STUDENT_1_EMAIL, STUDENT_1_PSW, Set.of(Role.ROLE_STUDENT), LocalDate.of(1996, 05, 22),
            2014, new NamedTo(TEAM_ID_1, TEAM_NAME_1), LocalDate.of(2008, 11, 11), null, "+7 123 123-45-67",
            "г.Пермь, ул.Футболистов, д.4", "КМС", null, 182, 65, 42, 96, 72, 85,
            "foot1.jpg", COACH_NAMED_1, TEAM_NAMED_1);

    public static final StudentInfoTo STUDENT_INFO_2 = new StudentInfoTo("Петров Петр Петрович",
            "student1@mail.ru", "student1", Set.of(Role.ROLE_STUDENT), LocalDate.of(1996, 05, 22),
            2014, new NamedTo(TEAM_ID_2, TEAM_NAME_2), LocalDate.of(2008, 11, 11), null, "+7 123 123-45-67",
            "г.Пермь, ул.Чемпионов, д.4", "МС", null, 182, 65, 42, 96, 72, 85,
            "xw_1547274.jpg", COACH_NAMED_1, TEAM_NAMED_1);

    public static final StudentInfoTo NEW_STUDENT = new StudentInfoTo("Новый студент", "newStudent@gmail.com", "strong", Set.of(Role.ROLE_STUDENT),
            LocalDate.of(1989, 4, 24), 2001, new NamedTo(TEAM_ID_2, TEAM_NAME_2), LocalDate.of(2000, 9, 1),
            LocalDate.of(2005, 5, 31), "+30231123213", "г Пермь ул Пушкина 2", "МС", null,
            175, 80, 43, 101, 50, 40, null, COACH_NAMED_1, TEAM_NAMED_1);

    public static final Team TEAM_1 = new Team();
    public static final Team TEAM_2 = new Team();
    public static final Team TEAM_3 = new Team();

    public static final User COACH_1_EDITED = new User(COACH_1);
    public static final String EDITED_NAME = "Измененное имя";

    static {
        TEAM_1.setId(TEAM_ID_1);
        TEAM_1.setName(TEAM_NAME_1);
        TEAM_1.setCoach(COACH_1);
        TEAM_2.setId(TEAM_ID_2);
        TEAM_2.setName(TEAM_NAME_2);
        TEAM_2.setCoach(COACH_2);
        TEAM_3.setId(TEAM_ID_3);
        TEAM_3.setName(TEAM_NAME_3);
        TEAM_3.setCoach(COACH_3);
        COACH_1.setCoachTeams(Set.of(TEAM_1, TEAM_2));
        NEW_COACH.setCoachTeams(Set.of(TEAM_2, TEAM_3));
        COACH_1_EDITED.setName(EDITED_NAME);
        COACH_1_EDITED.setCoachTeams(Set.of(TEAM_2, TEAM_3));
    }

    public static final User USER_STUDENT_1 = new User(STUDENT_ID_1, STUDENT_1_EMAIL, STUDENT_1_NAME, STUDENT_1_PSW, Set.of(Role.ROLE_STUDENT), TEAM_1);
    public static final User NEW_STUDENT_USER = new User(6, "newStudent@gmail.com", "Новый студент", "strong", Set.of(Role.ROLE_STUDENT), TEAM_1);

    static {
        STUDENT_INFO_1.setId(STUDENT_ID_1);
        STUDENT_INFO_2.setId(STUDENT_ID_2);
        NEW_STUDENT.setId(null);
    }

    public static final TeamSizedTo TEAM_SIZED_TO_1 = new TeamSizedTo(TEAM_ID_1, TEAM_NAME_1, null, 2);
    public static final TeamSizedTo TEAM_SIZED_TO_2 = new TeamSizedTo(TEAM_ID_2, TEAM_NAME_2, null, 1);
    public static final TeamSizedTo TEAM_SIZED_TO_3 = new TeamSizedTo(TEAM_ID_3, TEAM_NAME_3, null, 0);
    public static final CoachTo COACH_TO_1 = new CoachTo(COACH_ID_1, COACH_NAME_1, null, COACH_1.getStartpoint());
    public static final CoachTo COACH_TO_2 = new CoachTo(COACH_ID_2, COACH_NAME_2, null, COACH_2.getStartpoint());
    public static final CoachTo COACH_TO_3 = new CoachTo(COACH_ID_3, COACH_NAME_3, null, COACH_3.getStartpoint());

    static {
        COACH_TO_1.setTeams(List.of(TEAM_SIZED_TO_1, TEAM_SIZED_TO_2));
        COACH_TO_2.setTeams(List.of());
        COACH_TO_3.setTeams(List.of(TEAM_SIZED_TO_3));
    }

    public static final ArrayList<CoachTo> COACHES_TO = new ArrayList<>(); // cant use List.of here because assertion wants ArrayList not List!

    static {
        COACHES_TO.add(COACH_TO_3);
        COACHES_TO.add(COACH_TO_1);
        COACHES_TO.add(COACH_TO_2);
    }

    public static final List<NamedTo> COACHES_1_TEAMS = List.of(TEAM_NAMED_1, TEAM_NAMED_2);

    public static final TeamTo TEAM_TO_1 = new TeamTo(COACH_NAMED_1);

    static {
        TEAM_TO_1.setId(TEAM_ID_1);
        TEAM_TO_1.setName(TEAM_NAME_1);
    }

    public static final NamedTo MEMBER_1 = new NamedTo(STUDENT_ID_1, STUDENT_NAME_1);
    public static final NamedTo MEMBER_2 = new NamedTo(STUDENT_ID_2, STUDENT_NAME_2);
    public static final NamedTo MEMBER_3 = new NamedTo(STUDENT_ID_3, STUDENT_NAME_3);
    public static final TeamMembersTo TEAM_1_WITH_MEMBERS = new TeamMembersTo(TEAM_TO_1, List.of(MEMBER_1, MEMBER_2));
    public static final TeamMembersTo TEAM_1_UPDATED_WITH_MEMBERS = new TeamMembersTo(TEAM_TO_1, List.of(MEMBER_2, MEMBER_3));

    public static final CoachInfoTo NEW_COACH_INFO_TO = new CoachInfoTo(null, NEW_COACH.getName(), NEW_COACH.getImgName(), NEW_COACH.getEmail(), NEW_COACH.getPassword());
    public static final CoachInfoTo EDITED_COACH_INFO_TO_1 = new CoachInfoTo(COACH_ID_1, EDITED_NAME, null, COACH_1.getEmail(), COACH_1.getPassword());
    public static final CoachInfoTo COACH_INFO_TO_1 = new CoachInfoTo(COACH_ID_1, COACH_NAME_1, null, COACH_1.getEmail(), COACH_1.getPassword());

    static {
        NEW_COACH_INFO_TO.setTeams(List.of(TEAM_NAMED_2, TEAM_NAMED_3));
        EDITED_COACH_INFO_TO_1.setTeams(List.of(TEAM_NAMED_2, TEAM_NAMED_3));
        COACH_INFO_TO_1.setTeams(List.of(TEAM_NAMED_1, TEAM_NAMED_2));
    }

    public static final List<NamedTo> NAMED_TEAMS = List.of(TEAM_NAMED_1, TEAM_NAMED_2, TEAM_NAMED_3);
    public static final List<NamedTo> NAMED_COACHES = List.of(COACH_NAMED_3, COACH_NAMED_1, COACH_NAMED_2);

    public static <T> void assertMatch(T actual, T expected) {
        assertThat(actual).isEqualToComparingFieldByFieldRecursively(expected);
    }

    public static <T> void assertMatch(T actual, T expected, String... ignore) {
        assertThat(actual).usingComparatorForFields((x, y) -> 0, ignore).isEqualToComparingFieldByFieldRecursively(expected);
    }

    public static <T> void assertMatch(Iterable<T> actual, Iterable<T> expected, String... ignore) {
        assertThat(actual).usingElementComparatorIgnoringFields(ignore).isEqualTo(expected);
    }

    public static <T> void assertMatch(Iterable<T> actual, T... expected) {
        assertMatch(actual, List.of(expected), "imgName", "registered");
    }

    public static <T> ResultMatcher contentJson(JsonValueMapper jsonValueMapper, Iterable<T> expected, Class<T> clazz) {
        return result -> assertMatch(jsonValueMapper.readValues(getContent(result), clazz), expected, "registered", "imgName");
    }
}
