package space.pirs.modules.user.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.NamedTo;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TeamTo extends NamedTo {
    protected NamedTo coach;
}
