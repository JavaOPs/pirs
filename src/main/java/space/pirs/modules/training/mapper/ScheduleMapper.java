package space.pirs.modules.training.mapper;

import org.mapstruct.Mapper;
import space.pirs.BaseMapper;
import space.pirs.modules.training.model.Training;
import space.pirs.modules.training.to.ScheduleTo;

@Mapper(
        componentModel = "spring"
)
public abstract class ScheduleMapper implements BaseMapper<Training, ScheduleTo> {
}
