package space.pirs.modules.training.to;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.BaseTo;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class TrainingItemTo extends BaseTo {
    private int series;
    private int repeat;
    private int duration;
    private int pause;
    private ExerciseShortTo exercise;
    private Set<BaseTo> skills;

    public TrainingItemTo(Integer id, int series, int repeat, int duration, int pause, ExerciseShortTo exercise, Set<BaseTo> skills) {
        super(id);
        this.series = series;
        this.repeat = repeat;
        this.duration = duration;
        this.pause = pause;
        this.exercise = exercise;
        this.skills = Set.copyOf(skills);
    }
}
