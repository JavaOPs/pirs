package space.pirs.modules.user.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import space.pirs.modules.user.model.User;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.modules.user.service.CoachService;
import space.pirs.modules.user.to.CoachInfoTo;
import space.pirs.modules.user.to.CoachTo;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = AdminCoachController.REST_URL)
@AllArgsConstructor
public class AdminCoachController {
    static final String REST_URL = "/api/admin/coaches";

    private final CoachService service;
    private final UserRepository userRepository;

    @GetMapping
    public List<CoachTo> getAll() {
        log.info("getAll");
        return service.getAll();
    }

    @GetMapping("/{id}")
    public CoachInfoTo get(@PathVariable int id) {
        log.info("get {}", id);
        return service.get(id);
    }

    @PostMapping
    public User create(@RequestBody @Valid CoachInfoTo coach) {
        log.info("create {}", coach);
        return service.create(coach);
    }

    @PutMapping("/{id}")
    public void update(@RequestBody @Valid CoachInfoTo coach, @PathVariable int id) {
        log.info("update {} {}", coach, id);
        service.update(coach, id);
    }

    @PutMapping("/{id}/activate")
    public void activate(@PathVariable int id, @RequestParam boolean activate) {
        log.info("activate {} ", id);
        userRepository.activate(id, activate);
    }
}
