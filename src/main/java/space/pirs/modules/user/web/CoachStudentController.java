package space.pirs.modules.user.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import space.pirs.AuthUser;
import space.pirs.modules.user.mapper.StudentMapper;
import space.pirs.modules.user.repository.UserRepository;
import space.pirs.modules.user.service.StudentInfoService;
import space.pirs.modules.user.to.StudentInfoTo;
import space.pirs.modules.user.to.StudentTo;

import java.util.List;

@RestController
@RequestMapping(value = CoachStudentController.REST_URL , produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@AllArgsConstructor
@Slf4j
public class CoachStudentController {
    static final String REST_URL = "/api/coach/students";

    private final StudentInfoService studentInfoService;
    private final UserRepository userRepository;
    private final StudentMapper studentMapper;

    @GetMapping("/{studentId}")
    private StudentInfoTo getStudentInfo(@PathVariable int studentId, @AuthenticationPrincipal AuthUser coach) {
        log.info("getStudentInfo {} by {}", studentId, coach);
        return studentInfoService.get(studentId, coach.id());
    }

    @GetMapping("/by-team")
    public List<StudentTo> getByTeam(@RequestParam int teamId) {
        log.info("getByTeam {}", teamId);
        return studentMapper.toToList(userRepository.getAllByTeamId(teamId));
    }

    @GetMapping
    public List<StudentTo> getOwns(@AuthenticationPrincipal AuthUser coach) {
        log.info("getOwns by  {}", coach.id());
        return studentMapper.toToList(userRepository.getAllByCoachId(coach.id()));
    }
}