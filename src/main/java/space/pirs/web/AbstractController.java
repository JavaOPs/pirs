package space.pirs.web;

import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import space.pirs.BaseMapper;
import space.pirs.model.AbstractBaseEntity;
import space.pirs.repository.AbstractRepository;
import space.pirs.to.BaseTo;

import javax.validation.Valid;
import java.util.List;

public abstract class AbstractController<E extends AbstractBaseEntity, T extends BaseTo, R extends AbstractRepository<E>> extends BaseWebHandler<E, T, R> {

    public AbstractController(R repository, BaseMapper<E, T> mapper, Class controllerClass) {
        super(repository, mapper, controllerClass);
    }

    @GetMapping("/{id}")
    public T get(@PathVariable int id) {
        return super.get(id);
    }

    @GetMapping
    public List<T> getAll() {
        return super.getAll();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public E create(@RequestBody @Valid T to) {
        return super.create(to);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public void update(@RequestBody @Valid T to, @PathVariable int id) {
        super.update(to, id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        super.delete(id);
    }
}