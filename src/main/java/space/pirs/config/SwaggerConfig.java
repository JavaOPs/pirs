package space.pirs.config;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private static final String BASE_PACKAGE = "space.pirs";
    private static final String AUTH_NAME = "basic_auth";

    @Bean
    public Docket authenticatedApi() {
        return createDocket("Auth API", "any",
                or(regex("/api/profile.*"), regex("/api/skills.*"), regex("/api/info.*")));
    }

    @Bean
    public Docket adminApi() {
        return createDocket("Admin API", "admin", PathSelectors.ant("/api/admin/**"));
    }

    @Bean
    public Docket coachApi() {
        return createDocket("Coach API", "coach", PathSelectors.ant("/api/coach/**"));
    }

    @Bean
    public Docket studentApi() {
        return createDocket("Student API", "student", PathSelectors.ant("/api/student/**"));
    }

    @Bean
    public Docket publicApi() {
        return createDocket("Public API", "any", PathSelectors.ant("/public/**"));
    }

    private Docket createDocket(String name, String role, Predicate<String> paths) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(name)
                .securitySchemes(List.of(securityScheme()))
                .securityContexts(List.of(SecurityContext.builder()

//    there is ho way to configure API docs for "hasRole" security
//    https://stackoverflow.com/a/40222161/8856451
                        .securityReferences(List.of(new SecurityReference(AUTH_NAME, scope(role))))
                        .forPaths(paths)
                        .build()))
                .apiInfo(metaData())
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(paths)
                .build();
    }

    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("REST API documentation")
                .description("PIRS: cистема мониторинга показателей спортсменов")
                .version("1.0")
//            .license("Apache 2.0")
//            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
//            .contact(new Contact("Admin", "http://javaops.ru/", "admin@javaops.ru"))
                .build();
    }

    private SecurityScheme securityScheme() {
        return new BasicAuth(AUTH_NAME);
    }


    private AuthorizationScope[] scope(String scopeName) {
        AuthorizationScope[] scopes = new AuthorizationScope[1];
        scopes[0] = new AuthorizationScope(scopeName, "Access for role: " + scopeName);
        return scopes;
    }

    private AuthorizationScope[] scopes(String... scopeName) {
        AuthorizationScope[] scopes = new AuthorizationScope[scopeName.length];
        for (int i = 0; i < scopeName.length; i++) {
            scopes[i] = new AuthorizationScope(scopeName[i], "Access for role: " + scopeName[i]);
        }
        return scopes;
    }
}
