package space.pirs.modules.user.web;

import org.junit.Test;
import org.springframework.http.MediaType;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.user.to.StudentInfoTo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.getContent;
import static space.pirs.TestUtil.userHttpBasic;
import static space.pirs.modules.user.UserTestData.*;

public class StudentProfileControllerTest extends AbstractControllerTest {
    private static final String REST_URL = StudentProfileController.REST_URL + "/";

    @Test
    public void testGetStudentInfo() throws Exception {
        mockMvc.perform(get(REST_URL)
                .with(userHttpBasic(USER_STUDENT_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertMatch(jsonValueMapper.readValue(getContent(result), StudentInfoTo.class), STUDENT_INFO_1, "password"));
    }

    @Test
    public void testForbidden() throws Exception {
        mockMvc.perform(get(REST_URL)
                .with(userHttpBasic(COACH_1)))
                .andDo(print())
                .andExpect(status().isForbidden());

    }
}