package space.pirs.modules.training.to;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.to.BaseTo;
import space.pirs.to.NamedTo;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Builder(toBuilder = true)
public class TrainingInfoTo extends BaseTo {
    private LocalDate date;
    private LocalTime startTime;
    private LocalTime endTime;
    @NotNull
    private NamedTo team;
    private String comment;
    private List<TrainingItemTo> items;
    private Set<NamedTo> students;
    private boolean passed;

    public TrainingInfoTo(LocalDate date, LocalTime startTime, LocalTime endTime, NamedTo team, String comment,
                          List<TrainingItemTo> items, Set<NamedTo> students, boolean passed) {
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.team = team;
        this.comment = comment;
        this.items = List.copyOf(items);
        this.students = Set.copyOf(students);
        this.passed = passed;
    }
}
