package space.pirs.modules.health.model;

public enum PolicyType {
    OMS,
    DMS
}
