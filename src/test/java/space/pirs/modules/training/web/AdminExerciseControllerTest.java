package space.pirs.modules.training.web;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.web.util.NestedServletException;
import space.pirs.AbstractControllerTest;
import space.pirs.modules.training.to.ExerciseTo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static space.pirs.TestUtil.getContent;
import static space.pirs.TestUtil.userAuth;
import static space.pirs.modules.training.TrainingTestData.*;
import static space.pirs.modules.user.UserTestData.ADMIN;

public class AdminExerciseControllerTest extends AbstractControllerTest {
    private static final String REST_URL = AdminExerciseController.REST_URL + "/";

    @Test
    public void testGet() throws Exception {
        mockMvc.perform(get(REST_URL + EXERCISE_ID_1)
                .with(userAuth(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertMatch(jsonValueMapper.readValue(getContent(result), ExerciseTo.class), EXERCISE_1));
    }

    @Test
    public void testGetAll() throws Exception {
        mockMvc.perform(get(REST_URL)
                .with(userAuth(ADMIN)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(contentJson(jsonValueMapper, EXERCISE_TO_LIST, ExerciseTo.class));
    }

    @Test(expected = NestedServletException.class)
    public void testGetNotFound() throws Exception {
        mockMvc.perform(get(REST_URL + "0")
                .with(userAuth(ADMIN)))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void testGetNotAuth() throws Exception {
        mockMvc.perform(get(REST_URL))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}