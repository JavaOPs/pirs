package space.pirs.modules.training.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import space.pirs.model.AbstractBaseEntity;
import space.pirs.modules.skill.model.Skill;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@NoArgsConstructor
@Table(name = "exercise")
@Getter
@Setter
public class Exercise extends AbstractBaseEntity {

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id", nullable = false)
    @NotNull
    private ExerciseGroup group;

    @Column(name = "description")
    @Size(max = 1024)
    private String description;

    @Column(name = "circumstance")
    @Size(max = 1024)
    private String circumstance;

    @ManyToMany
    @JoinTable(
            name = "exercise_skill",
            joinColumns = @JoinColumn(name = "exercise_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id"))
    private Set<Skill> skills;
}
